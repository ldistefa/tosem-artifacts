typedef char TYPEOFVALUES;
typedef unsigned char TYPEOFPC;
typedef unsigned char TYPEOFTIME;
typedef unsigned char TYPEOFAGENTID;
typedef unsigned char TYPEOFKEYIID;
typedef unsigned char TYPEOFKEYLID;
extern int __VERIFIER_nondet_int(void);
extern void __VERIFIER_assume(int);
TYPEOFTIME __LABS_time;
TYPEOFVALUES I_0;
TYPEOFVALUES I_1;
TYPEOFVALUES I_2;
TYPEOFVALUES I_3;
TYPEOFVALUES I_4;
TYPEOFVALUES I_5;
TYPEOFPC pc_0;
TYPEOFPC pc_1;
TYPEOFPC pc_2;
_Bool Hin_0;
_Bool Hin_1;
_Bool Hin_2;
_Bool Hin_3;
_Bool Hin_4;
_Bool Hin_5;
_Bool Hin_6;
_Bool Hin_7;
_Bool Hin_8;
_Bool Hin_9;
_Bool Hin_10;
_Bool Hin_11;
_Bool Hin_12;
_Bool Hin_13;
_Bool Hin_14;
_Bool Hin_15;
_Bool Hin_16;
_Bool Hin_17;
_Bool Hout_0;
_Bool Hout_1;
_Bool Hout_2;
_Bool Hout_3;
_Bool Hout_4;
_Bool Hout_5;
_Bool Hout_6;
_Bool Hout_7;
_Bool Hout_8;
_Bool Hout_9;
_Bool Hout_10;
_Bool Hout_11;
_Bool Hout_12;
_Bool Hout_13;
_Bool Hout_14;
_Bool Hout_15;
_Bool Hout_16;
_Bool Hout_17;
unsigned char HinCnt_0;
unsigned char HinCnt_1;
unsigned char HinCnt_2;
unsigned char HoutCnt_0;
unsigned char HoutCnt_1;
unsigned char HoutCnt_2;
TYPEOFVALUES Lvalue_0;
TYPEOFVALUES Lvalue_1;
TYPEOFVALUES Lvalue_2;
TYPEOFVALUES Lvalue_3;
TYPEOFVALUES Lvalue_4;
TYPEOFVALUES Lvalue_5;
TYPEOFVALUES Lvalue_6;
TYPEOFVALUES Lvalue_7;
TYPEOFVALUES Lvalue_8;
TYPEOFVALUES Lvalue_9;
TYPEOFVALUES Lvalue_10;
TYPEOFVALUES Lvalue_11;
TYPEOFVALUES Lvalue_12;
TYPEOFVALUES Lvalue_13;
TYPEOFVALUES Lvalue_14;
TYPEOFVALUES Lvalue_15;
TYPEOFVALUES Lvalue_16;
TYPEOFVALUES Lvalue_17;
TYPEOFTIME Ltstamp_0;
TYPEOFTIME Ltstamp_1;
TYPEOFTIME Ltstamp_2;
TYPEOFTIME Ltstamp_3;
TYPEOFTIME Ltstamp_4;
TYPEOFTIME Ltstamp_5;
TYPEOFTIME Ltstamp_6;
TYPEOFTIME Ltstamp_7;
TYPEOFTIME Ltstamp_8;
TYPEOFTIME Ltstamp_9;
TYPEOFTIME Ltstamp_10;
TYPEOFTIME Ltstamp_11;
TYPEOFTIME Ltstamp_12;
TYPEOFTIME Ltstamp_13;
TYPEOFTIME Ltstamp_14;
TYPEOFTIME Ltstamp_15;
TYPEOFTIME Ltstamp_16;
TYPEOFTIME Ltstamp_17;
const TYPEOFKEYLID tupleStart_0 = 0;
const TYPEOFKEYLID tupleStart_1 = 0;
const TYPEOFKEYLID tupleStart_2 = 0;
const TYPEOFKEYLID tupleStart_3 = 0;
const TYPEOFKEYLID tupleStart_4 = 4;
const TYPEOFKEYLID tupleStart_5 = 4;
const TYPEOFKEYLID tupleEnd_0 = 3;
const TYPEOFKEYLID tupleEnd_1 = 3;
const TYPEOFKEYLID tupleEnd_2 = 3;
const TYPEOFKEYLID tupleEnd_3 = 3;
const TYPEOFKEYLID tupleEnd_4 = 5;
const TYPEOFKEYLID tupleEnd_5 = 5;
TYPEOFVALUES _I_170_1_0;
TYPEOFVALUES _I_170_1_1;
TYPEOFVALUES _I_170_1_2;
TYPEOFVALUES _I_170_1_3;
TYPEOFVALUES _I_170_1_4;
TYPEOFVALUES _I_170_1_5;
TYPEOFPC _pc_170_1_0;
TYPEOFPC _pc_170_1_1;
TYPEOFPC _pc_170_1_2;
TYPEOFVALUES _Lvalue_170_1_0;
TYPEOFVALUES _Lvalue_170_1_1;
TYPEOFVALUES _Lvalue_170_1_2;
TYPEOFVALUES _Lvalue_170_1_3;
TYPEOFVALUES _Lvalue_170_1_4;
TYPEOFVALUES _Lvalue_170_1_5;
TYPEOFVALUES _Lvalue_170_1_6;
TYPEOFVALUES _Lvalue_170_1_7;
TYPEOFVALUES _Lvalue_170_1_8;
TYPEOFVALUES _Lvalue_170_1_9;
TYPEOFVALUES _Lvalue_170_1_10;
TYPEOFVALUES _Lvalue_170_1_11;
TYPEOFVALUES _Lvalue_170_1_12;
TYPEOFVALUES _Lvalue_170_1_13;
TYPEOFVALUES _Lvalue_170_1_14;
TYPEOFVALUES _Lvalue_170_1_15;
TYPEOFVALUES _Lvalue_170_1_16;
TYPEOFVALUES _Lvalue_170_1_17;
TYPEOFVALUES __abs(TYPEOFVALUES x)
{
  return (x > 0) ? (x) : (-x);
}

TYPEOFVALUES mod(TYPEOFVALUES n, TYPEOFVALUES m)
{
  return (n >= 0) ? (n % m) : (m + (n % m));
}

TYPEOFTIME now(void)
{
  return ++__LABS_time;
}

unsigned char getHinCnt(int x0)
{
  switch (x0)
  {
    case 0:
      return HinCnt_0;

    case 1:
      return HinCnt_1;

    case 2:
      return HinCnt_2;

  }

}

void set_Lvalue_170_1(int x0, int x1, TYPEOFVALUES value)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      _Lvalue_170_1_0 = value;
      break;

    case 1:
      _Lvalue_170_1_1 = value;
      break;

    case 2:
      _Lvalue_170_1_2 = value;
      break;

    case 3:
      _Lvalue_170_1_3 = value;
      break;

    case 4:
      _Lvalue_170_1_4 = value;
      break;

    case 5:
      _Lvalue_170_1_5 = value;
      break;

    case 6:
      _Lvalue_170_1_6 = value;
      break;

    case 7:
      _Lvalue_170_1_7 = value;
      break;

    case 8:
      _Lvalue_170_1_8 = value;
      break;

    case 9:
      _Lvalue_170_1_9 = value;
      break;

    case 10:
      _Lvalue_170_1_10 = value;
      break;

    case 11:
      _Lvalue_170_1_11 = value;
      break;

    case 12:
      _Lvalue_170_1_12 = value;
      break;

    case 13:
      _Lvalue_170_1_13 = value;
      break;

    case 14:
      _Lvalue_170_1_14 = value;
      break;

    case 15:
      _Lvalue_170_1_15 = value;
      break;

    case 16:
      _Lvalue_170_1_16 = value;
      break;

    case 17:
      _Lvalue_170_1_17 = value;
      break;

  }

}

void setLtstamp(int x0, int x1, TYPEOFTIME value)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      Ltstamp_0 = value;
      break;

    case 1:
      Ltstamp_1 = value;
      break;

    case 2:
      Ltstamp_2 = value;
      break;

    case 3:
      Ltstamp_3 = value;
      break;

    case 4:
      Ltstamp_4 = value;
      break;

    case 5:
      Ltstamp_5 = value;
      break;

    case 6:
      Ltstamp_6 = value;
      break;

    case 7:
      Ltstamp_7 = value;
      break;

    case 8:
      Ltstamp_8 = value;
      break;

    case 9:
      Ltstamp_9 = value;
      break;

    case 10:
      Ltstamp_10 = value;
      break;

    case 11:
      Ltstamp_11 = value;
      break;

    case 12:
      Ltstamp_12 = value;
      break;

    case 13:
      Ltstamp_13 = value;
      break;

    case 14:
      Ltstamp_14 = value;
      break;

    case 15:
      Ltstamp_15 = value;
      break;

    case 16:
      Ltstamp_16 = value;
      break;

    case 17:
      Ltstamp_17 = value;
      break;

  }

}

TYPEOFKEYLID gettupleEnd(int x0)
{
  switch (x0)
  {
    case 0:
      return tupleEnd_0;

    case 1:
      return tupleEnd_1;

    case 2:
      return tupleEnd_2;

    case 3:
      return tupleEnd_3;

    case 4:
      return tupleEnd_4;

    case 5:
      return tupleEnd_5;

  }

}

void setpc(int x0, int x1, TYPEOFPC value)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      pc_0 = value;
      break;

    case 1:
      pc_1 = value;
      break;

    case 2:
      pc_2 = value;
      break;

  }

}

void set_pc_170_1(int x0, int x1, TYPEOFPC value)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      _pc_170_1_0 = value;
      break;

    case 1:
      _pc_170_1_1 = value;
      break;

    case 2:
      _pc_170_1_2 = value;
      break;

  }

}

TYPEOFVALUES get_Lvalue_170_1(int x0, int x1)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      return _Lvalue_170_1_0;

    case 1:
      return _Lvalue_170_1_1;

    case 2:
      return _Lvalue_170_1_2;

    case 3:
      return _Lvalue_170_1_3;

    case 4:
      return _Lvalue_170_1_4;

    case 5:
      return _Lvalue_170_1_5;

    case 6:
      return _Lvalue_170_1_6;

    case 7:
      return _Lvalue_170_1_7;

    case 8:
      return _Lvalue_170_1_8;

    case 9:
      return _Lvalue_170_1_9;

    case 10:
      return _Lvalue_170_1_10;

    case 11:
      return _Lvalue_170_1_11;

    case 12:
      return _Lvalue_170_1_12;

    case 13:
      return _Lvalue_170_1_13;

    case 14:
      return _Lvalue_170_1_14;

    case 15:
      return _Lvalue_170_1_15;

    case 16:
      return _Lvalue_170_1_16;

    case 17:
      return _Lvalue_170_1_17;

  }

}

TYPEOFVALUES getLvalue(int x0, int x1)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      return Lvalue_0;

    case 1:
      return Lvalue_1;

    case 2:
      return Lvalue_2;

    case 3:
      return Lvalue_3;

    case 4:
      return Lvalue_4;

    case 5:
      return Lvalue_5;

    case 6:
      return Lvalue_6;

    case 7:
      return Lvalue_7;

    case 8:
      return Lvalue_8;

    case 9:
      return Lvalue_9;

    case 10:
      return Lvalue_10;

    case 11:
      return Lvalue_11;

    case 12:
      return Lvalue_12;

    case 13:
      return Lvalue_13;

    case 14:
      return Lvalue_14;

    case 15:
      return Lvalue_15;

    case 16:
      return Lvalue_16;

    case 17:
      return Lvalue_17;

  }

}

void setI(int x0, int x1, TYPEOFVALUES value)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      I_0 = value;
      break;

    case 1:
      I_1 = value;
      break;

    case 2:
      I_2 = value;
      break;

    case 3:
      I_3 = value;
      break;

    case 4:
      I_4 = value;
      break;

    case 5:
      I_5 = value;
      break;

  }

}

TYPEOFTIME getLtstamp(int x0, int x1)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      return Ltstamp_0;

    case 1:
      return Ltstamp_1;

    case 2:
      return Ltstamp_2;

    case 3:
      return Ltstamp_3;

    case 4:
      return Ltstamp_4;

    case 5:
      return Ltstamp_5;

    case 6:
      return Ltstamp_6;

    case 7:
      return Ltstamp_7;

    case 8:
      return Ltstamp_8;

    case 9:
      return Ltstamp_9;

    case 10:
      return Ltstamp_10;

    case 11:
      return Ltstamp_11;

    case 12:
      return Ltstamp_12;

    case 13:
      return Ltstamp_13;

    case 14:
      return Ltstamp_14;

    case 15:
      return Ltstamp_15;

    case 16:
      return Ltstamp_16;

    case 17:
      return Ltstamp_17;

  }

}

TYPEOFVALUES get_I_170_1(int x0, int x1)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      return _I_170_1_0;

    case 1:
      return _I_170_1_1;

    case 2:
      return _I_170_1_2;

    case 3:
      return _I_170_1_3;

    case 4:
      return _I_170_1_4;

    case 5:
      return _I_170_1_5;

  }

}

_Bool _getHout(int x0, int x1)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      return Hout_0;

    case 1:
      return Hout_1;

    case 2:
      return Hout_2;

    case 3:
      return Hout_3;

    case 4:
      return Hout_4;

    case 5:
      return Hout_5;

    case 6:
      return Hout_6;

    case 7:
      return Hout_7;

    case 8:
      return Hout_8;

    case 9:
      return Hout_9;

    case 10:
      return Hout_10;

    case 11:
      return Hout_11;

    case 12:
      return Hout_12;

    case 13:
      return Hout_13;

    case 14:
      return Hout_14;

    case 15:
      return Hout_15;

    case 16:
      return Hout_16;

    case 17:
      return Hout_17;

  }

}

TYPEOFKEYLID gettupleStart(int x0)
{
  switch (x0)
  {
    case 0:
      return tupleStart_0;

    case 1:
      return tupleStart_1;

    case 2:
      return tupleStart_2;

    case 3:
      return tupleStart_3;

    case 4:
      return tupleStart_4;

    case 5:
      return tupleStart_5;

  }

}

TYPEOFTIME timeof(TYPEOFAGENTID id, TYPEOFKEYLID key)
{
  return getLtstamp(id, gettupleStart(key));
}

void setLvalue(int x0, int x1, TYPEOFVALUES value)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      Lvalue_0 = value;
      break;

    case 1:
      Lvalue_1 = value;
      break;

    case 2:
      Lvalue_2 = value;
      break;

    case 3:
      Lvalue_3 = value;
      break;

    case 4:
      Lvalue_4 = value;
      break;

    case 5:
      Lvalue_5 = value;
      break;

    case 6:
      Lvalue_6 = value;
      break;

    case 7:
      Lvalue_7 = value;
      break;

    case 8:
      Lvalue_8 = value;
      break;

    case 9:
      Lvalue_9 = value;
      break;

    case 10:
      Lvalue_10 = value;
      break;

    case 11:
      Lvalue_11 = value;
      break;

    case 12:
      Lvalue_12 = value;
      break;

    case 13:
      Lvalue_13 = value;
      break;

    case 14:
      Lvalue_14 = value;
      break;

    case 15:
      Lvalue_15 = value;
      break;

    case 16:
      Lvalue_16 = value;
      break;

    case 17:
      Lvalue_17 = value;
      break;

  }

}

void setHinCnt(int x0, unsigned value)
{
  switch (x0)
  {
    case 0:
      HinCnt_0 = value;
      break;

    case 1:
      HinCnt_1 = value;
      break;

    case 2:
      HinCnt_2 = value;
      break;

  }

}

TYPEOFVALUES getI(int x0, int x1)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      return I_0;

    case 1:
      return I_1;

    case 2:
      return I_2;

    case 3:
      return I_3;

    case 4:
      return I_4;

    case 5:
      return I_5;

  }

}

_Bool link(TYPEOFAGENTID __LABS_link1, TYPEOFAGENTID __LABS_link2, TYPEOFKEYLID key)
{
  _Bool __LABS_link = 0;
  if ((key >= 4) && (key <= 5))
  {
    __LABS_link = (((getI(__LABS_link1, 0) - getI(__LABS_link2, 0)) * (getI(__LABS_link1, 0) - getI(__LABS_link2, 0))) + ((getI(__LABS_link1, 1) - getI(__LABS_link2, 1)) * (getI(__LABS_link1, 1) - getI(__LABS_link2, 1)))) <= (5 * 5);
  }
  else
    if ((key >= 0) && (key <= 3))
  {
    __LABS_link = getLvalue(__LABS_link1, 0) >= getLvalue(__LABS_link2, 0);
  }


  return __LABS_link;
}

TYPEOFPC getpc(int x0, int x1)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      return pc_0;

    case 1:
      return pc_1;

    case 2:
      return pc_2;

  }

}

_Bool _getHin(int x0, int x1)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      return Hin_0;

    case 1:
      return Hin_1;

    case 2:
      return Hin_2;

    case 3:
      return Hin_3;

    case 4:
      return Hin_4;

    case 5:
      return Hin_5;

    case 6:
      return Hin_6;

    case 7:
      return Hin_7;

    case 8:
      return Hin_8;

    case 9:
      return Hin_9;

    case 10:
      return Hin_10;

    case 11:
      return Hin_11;

    case 12:
      return Hin_12;

    case 13:
      return Hin_13;

    case 14:
      return Hin_14;

    case 15:
      return Hin_15;

    case 16:
      return Hin_16;

    case 17:
      return Hin_17;

  }

}

void _setHin(int x0, int x1, _Bool value)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      Hin_0 = value;
      break;

    case 1:
      Hin_1 = value;
      break;

    case 2:
      Hin_2 = value;
      break;

    case 3:
      Hin_3 = value;
      break;

    case 4:
      Hin_4 = value;
      break;

    case 5:
      Hin_5 = value;
      break;

    case 6:
      Hin_6 = value;
      break;

    case 7:
      Hin_7 = value;
      break;

    case 8:
      Hin_8 = value;
      break;

    case 9:
      Hin_9 = value;
      break;

    case 10:
      Hin_10 = value;
      break;

    case 11:
      Hin_11 = value;
      break;

    case 12:
      Hin_12 = value;
      break;

    case 13:
      Hin_13 = value;
      break;

    case 14:
      Hin_14 = value;
      break;

    case 15:
      Hin_15 = value;
      break;

    case 16:
      Hin_16 = value;
      break;

    case 17:
      Hin_17 = value;
      break;

  }

}

void setHin(TYPEOFAGENTID id, TYPEOFKEYLID key)
{
  setHinCnt(id, getHinCnt(id) + (!_getHin(id, gettupleStart(key))));
  _setHin(id, gettupleStart(key), 1);
}

void clearHin(TYPEOFAGENTID id, TYPEOFKEYLID key)
{
  setHinCnt(id, getHinCnt(id) - _getHin(id, gettupleStart(key)));
  _setHin(id, gettupleStart(key), 0);
}

TYPEOFPC get_pc_170_1(int x0, int x1)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      return _pc_170_1_0;

    case 1:
      return _pc_170_1_1;

    case 2:
      return _pc_170_1_2;

  }

}

unsigned char getHoutCnt(int x0)
{
  switch (x0)
  {
    case 0:
      return HoutCnt_0;

    case 1:
      return HoutCnt_1;

    case 2:
      return HoutCnt_2;

  }

}

void _0_6(int tid)
{
  __VERIFIER_assume((getHoutCnt(tid) == 0) && (getHinCnt(tid) == 0));
  __VERIFIER_assume(__abs(getI(tid, 0) - getLvalue(tid, 2)) <= 5);
  setHin(tid, 2);
  TYPEOFPC pc0 = __VERIFIER_nondet_int();
  __VERIFIER_assume((pc0 == 3) || (pc0 == 4));
  setpc(tid, 0, pc0);
}

void _0_4(int tid)
{
  __VERIFIER_assume((getHoutCnt(tid) == 0) && (getHinCnt(tid) == 0));
  __VERIFIER_assume(__abs(getI(tid, 1) - getLvalue(tid, 3)) <= 5);
  setHin(tid, 3);
  setpc(tid, 0, 8);
}

void attr(TYPEOFAGENTID id, TYPEOFKEYIID key, TYPEOFVALUES value, _Bool check)
{
  __VERIFIER_assume((!check) || (getHoutCnt(id) == 0));
  __VERIFIER_assume((!check) || (getHinCnt(id) == 0));
  setI(id, key, value);
  now();
}

void _0_8(int tid)
{
  TYPEOFVALUES val0 = mod(getI(tid, 0) + getLvalue(tid, 4), 5);
  TYPEOFVALUES val1 = mod(getI(tid, 1) + getLvalue(tid, 5), 5);
  attr(tid, 0, val0, 1);
  attr(tid, 1, val1, 0);
  setHin(tid, 4);
  setHin(tid, 5);
  TYPEOFPC pc0 = __VERIFIER_nondet_int();
  __VERIFIER_assume((pc0 == 2) || (pc0 == 7));
  setpc(tid, 0, pc0);
}

void set_I_170_1(int x0, int x1, TYPEOFVALUES value)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      _I_170_1_0 = value;
      break;

    case 1:
      _I_170_1_1 = value;
      break;

    case 2:
      _I_170_1_2 = value;
      break;

    case 3:
      _I_170_1_3 = value;
      break;

    case 4:
      _I_170_1_4 = value;
      break;

    case 5:
      _I_170_1_5 = value;
      break;

  }

}

void _setHout(int x0, int x1, _Bool value)
{
  switch ((1 * x0) + x1)
  {
    case 0:
      Hout_0 = value;
      break;

    case 1:
      Hout_1 = value;
      break;

    case 2:
      Hout_2 = value;
      break;

    case 3:
      Hout_3 = value;
      break;

    case 4:
      Hout_4 = value;
      break;

    case 5:
      Hout_5 = value;
      break;

    case 6:
      Hout_6 = value;
      break;

    case 7:
      Hout_7 = value;
      break;

    case 8:
      Hout_8 = value;
      break;

    case 9:
      Hout_9 = value;
      break;

    case 10:
      Hout_10 = value;
      break;

    case 11:
      Hout_11 = value;
      break;

    case 12:
      Hout_12 = value;
      break;

    case 13:
      Hout_13 = value;
      break;

    case 14:
      Hout_14 = value;
      break;

    case 15:
      Hout_15 = value;
      break;

    case 16:
      Hout_16 = value;
      break;

    case 17:
      Hout_17 = value;
      break;

  }

}

void setHoutCnt(int x0, unsigned value)
{
  switch (x0)
  {
    case 0:
      HoutCnt_0 = value;
      break;

    case 1:
      HoutCnt_1 = value;
      break;

    case 2:
      HoutCnt_2 = value;
      break;

  }

}

void clearHout(TYPEOFAGENTID id, TYPEOFKEYLID key)
{
  setHoutCnt(id, getHoutCnt(id) - _getHout(id, gettupleStart(key)));
  _setHout(id, gettupleStart(key), 0);
}

void setHout(TYPEOFAGENTID id, TYPEOFKEYLID key)
{
  setHoutCnt(id, getHoutCnt(id) + (!_getHout(id, gettupleStart(key))));
  _setHout(id, gettupleStart(key), 1);
}

void propagate(void)
{
  TYPEOFAGENTID guessedcomp = __VERIFIER_nondet_int();
  __VERIFIER_assume(guessedcomp < 3);
  __VERIFIER_assume(getHoutCnt(guessedcomp) > 0);
  TYPEOFKEYLID guessedkey = __VERIFIER_nondet_int();
  __VERIFIER_assume(guessedkey < 6);
  __VERIFIER_assume(_getHout(guessedcomp, guessedkey) == 1);
  TYPEOFAGENTID i = __VERIFIER_nondet_int();
  TYPEOFTIME t = timeof(guessedcomp, guessedkey);
  for (i = 0; i < 3; i++)
  {
    if (((guessedcomp != i) && (timeof(i, guessedkey) < t)) && link(guessedcomp, i, guessedkey))
    {
      setHout(i, guessedkey);
      clearHin(i, guessedkey);
      TYPEOFKEYLID k = __VERIFIER_nondet_int();
      TYPEOFKEYLID next = __VERIFIER_nondet_int();
      for (k = 0; k < 4; k++)
      {
        next = guessedkey + k;
        if (next <= gettupleEnd(guessedkey))
          setLvalue(i, next, getLvalue(guessedcomp, next));

      }

      setLtstamp(i, guessedkey, t);
    }

  }

  clearHout(guessedcomp, guessedkey);
}

void lstig(TYPEOFAGENTID id, TYPEOFKEYLID key, TYPEOFVALUES value, _Bool check)
{
  __VERIFIER_assume((!check) || (getHoutCnt(id) == 0));
  __VERIFIER_assume((!check) || (getHinCnt(id) == 0));
  setLvalue(id, key, value);
  setLtstamp(id, gettupleStart(key), now());
  setHout(id, key);
}

void _0_2(int tid)
{
  __VERIFIER_assume(getLvalue(tid, 1) == tid);
  TYPEOFVALUES val0 = getI(tid, 0);
  TYPEOFVALUES val1 = getI(tid, 1);
  lstig(tid, 2, val0, 1);
  lstig(tid, 3, val1, 0);
  setHin(tid, 1);
  setpc(tid, 0, 8);
}

void _0_7(int tid)
{
  __VERIFIER_assume(getLvalue(tid, 1) != tid);
  TYPEOFVALUES val0 = getLvalue(tid, 0) + 1;
  lstig(tid, 0, val0, 1);
  setHin(tid, 0);
  setHin(tid, 1);
  TYPEOFPC pc0 = __VERIFIER_nondet_int();
  __VERIFIER_assume((pc0 == 5) || (pc0 == 6));
  setpc(tid, 0, pc0);
}

void _0_5(int tid)
{
  __VERIFIER_assume(__abs(getI(tid, 0) - getLvalue(tid, 2)) > 5);
  TYPEOFVALUES val0 = (getLvalue(tid, 2) - getI(tid, 0)) / __abs(getLvalue(tid, 2) - getI(tid, 0));
  lstig(tid, 4, val0, 1);
  setHin(tid, 2);
  TYPEOFPC pc0 = __VERIFIER_nondet_int();
  __VERIFIER_assume((pc0 == 3) || (pc0 == 4));
  setpc(tid, 0, pc0);
}

void _0_3(int tid)
{
  __VERIFIER_assume(__abs(getI(tid, 1) - getLvalue(tid, 3)) > 5);
  TYPEOFVALUES val0 = (getLvalue(tid, 3) - getI(tid, 1)) / __abs(getLvalue(tid, 3) - getI(tid, 1));
  lstig(tid, 5, val0, 1);
  setHin(tid, 3);
  setpc(tid, 0, 8);
}

void confirm(void)
{
  TYPEOFAGENTID guessedcomp = __VERIFIER_nondet_int();
  __VERIFIER_assume(guessedcomp < 3);
  __VERIFIER_assume(getHinCnt(guessedcomp) > 0);
  TYPEOFKEYLID guessedkey = __VERIFIER_nondet_int();
  __VERIFIER_assume(guessedkey < 6);
  __VERIFIER_assume(_getHin(guessedcomp, guessedkey) == 1);
  TYPEOFAGENTID i = __VERIFIER_nondet_int();
  TYPEOFTIME t = timeof(guessedcomp, guessedkey);
  for (i = 0; i < 3; i++)
  {
    if (((guessedcomp != i) && (timeof(i, guessedkey) != t)) && link(guessedcomp, i, guessedkey))
    {
      setHout(i, guessedkey);
      if (timeof(i, guessedkey) < t)
      {
        TYPEOFKEYLID k = __VERIFIER_nondet_int();
        TYPEOFKEYLID next = __VERIFIER_nondet_int();
        clearHin(i, guessedkey);
        for (k = 0; k < 4; k++)
        {
          next = guessedkey + k;
          if (next <= gettupleEnd(guessedkey))
            setLvalue(i, next, getLvalue(guessedcomp, next));

        }

        setLtstamp(i, guessedkey, t);
      }

    }

  }

  clearHin(guessedcomp, guessedkey);
}

void init()
{
  ;
  ;
  ;
  unsigned char i = __VERIFIER_nondet_int();
  unsigned char j = __VERIFIER_nondet_int();
  for (i = 0; i < 3; i++)
  {
    for (j = 0; j < 2; j++)
    {
      set_I_170_1(i, j, __VERIFIER_nondet_int());
    }

    for (j = 0; j < 6; j++)
    {
      set_Lvalue_170_1(i, j, __VERIFIER_nondet_int());
      setLtstamp(i, j, 0);
      _setHin(i, j, 0);
      _setHout(i, j, 0);
    }

    setHinCnt(i, 0);
    setHoutCnt(i, 0);
  }

  set_pc_170_1(0, 0, 8);
  set_pc_170_1(1, 0, 8);
  set_pc_170_1(2, 0, 8);
  __VERIFIER_assume(get_Lvalue_170_1(0, 0) == 1);
  __VERIFIER_assume(get_Lvalue_170_1(1, 0) == 1);
  __VERIFIER_assume(get_Lvalue_170_1(2, 0) == 1);
  __VERIFIER_assume(get_Lvalue_170_1(0, 1) == 0);
  //set_Lvalue_170_1(1, 1, __VERIFIER_nondet_int());
  __VERIFIER_assume(get_Lvalue_170_1(1, 1) == 1);
  __VERIFIER_assume(get_Lvalue_170_1(2, 1) == 2);
  __VERIFIER_assume(get_Lvalue_170_1(0, 2) == (-1));
  __VERIFIER_assume(get_Lvalue_170_1(1, 2) == (-1));
  __VERIFIER_assume(get_Lvalue_170_1(2, 2) == (-1));
  __VERIFIER_assume(get_Lvalue_170_1(0, 3) == (-1));
  __VERIFIER_assume(get_Lvalue_170_1(1, 3) == (-1));
  __VERIFIER_assume(get_Lvalue_170_1(2, 3) == (-1));
  __VERIFIER_assume((get_Lvalue_170_1(0, 4) == (-1)) || (get_Lvalue_170_1(0, 4) == 1));
  __VERIFIER_assume((get_Lvalue_170_1(1, 4) == (-1)) || (get_Lvalue_170_1(1, 4) == 1));
  __VERIFIER_assume((get_Lvalue_170_1(2, 4) == (-1)) || (get_Lvalue_170_1(2, 4) == 1));
  __VERIFIER_assume((get_Lvalue_170_1(0, 5) == (-1)) || (get_Lvalue_170_1(0, 5) == 1));
  __VERIFIER_assume((get_Lvalue_170_1(1, 5) == (-1)) || (get_Lvalue_170_1(1, 5) == 1));
  __VERIFIER_assume((get_Lvalue_170_1(2, 5) == (-1)) || (get_Lvalue_170_1(2, 5) == 1));
  __VERIFIER_assume((get_I_170_1(0, 0) >= 0) && (get_I_170_1(0, 0) < 5));
  __VERIFIER_assume((get_I_170_1(1, 0) >= 0) && (get_I_170_1(1, 0) < 5));
  __VERIFIER_assume((get_I_170_1(2, 0) >= 0) && (get_I_170_1(2, 0) < 5));
  __VERIFIER_assume((get_I_170_1(0, 1) >= 0) && (get_I_170_1(0, 1) < 5));
  __VERIFIER_assume((get_I_170_1(1, 1) >= 0) && (get_I_170_1(1, 1) < 5));
  __VERIFIER_assume((get_I_170_1(2, 1) >= 0) && (get_I_170_1(2, 1) < 5));
  setLtstamp(0, gettupleStart(0), now());
  setLtstamp(1, gettupleStart(0), now());
  setLtstamp(2, gettupleStart(0), now());
  setLtstamp(0, gettupleStart(1), now());
  setLtstamp(1, gettupleStart(1), now());
  setLtstamp(2, gettupleStart(1), now());
  setLtstamp(0, gettupleStart(2), now());
  setLtstamp(1, gettupleStart(2), now());
  setLtstamp(2, gettupleStart(2), now());
  setLtstamp(0, gettupleStart(3), now());
  setLtstamp(1, gettupleStart(3), now());
  setLtstamp(2, gettupleStart(3), now());
  setLtstamp(0, gettupleStart(4), now());
  setLtstamp(1, gettupleStart(4), now());
  setLtstamp(2, gettupleStart(4), now());
  setLtstamp(0, gettupleStart(5), now());
  setLtstamp(1, gettupleStart(5), now());
  setLtstamp(2, gettupleStart(5), now());
  now();
  for (i = 0; i < 3; i++)
  {
    for (j = 0; j < 1; j++)
    {
      setpc(i, j, get_pc_170_1(i, j));
    }

    for (j = 0; j < 2; j++)
    {
      setI(i, j, get_I_170_1(i, j));
    }

    for (j = 0; j < 6; j++)
    {
      setLvalue(i, j, get_Lvalue_170_1(i, j));
    }

  }

}

int main(void)
{
  init();
  TYPEOFAGENTID firstAgent = 0;
  while (1)
  {
    if ((_Bool) __VERIFIER_nondet_int())
    {
      switch (getpc(firstAgent, 0))
      {
        case 2:
          _0_2(firstAgent);
          break;

        case 3:
          _0_3(firstAgent);
          break;

        case 4:
          _0_4(firstAgent);
          break;

        case 5:
          _0_5(firstAgent);
          break;

        case 6:
          _0_6(firstAgent);
          break;

        case 7:
          _0_7(firstAgent);
          break;

        case 8:
          _0_8(firstAgent);
          break;

        default:
        {
        }

      }

      if (firstAgent == (3 - 1))
      {
        firstAgent = 0;
      }
      else
      {
        firstAgent++;
      }

    }
    else
    {
      _Bool propagate_or_confirm = __VERIFIER_nondet_int();
      if (propagate_or_confirm)
        propagate();
      else
        confirm();

    }

    if ((((((getLvalue(0, 1) == getLvalue(0, 1)) && (getLvalue(0, 1) == getLvalue(1, 1))) && (getLvalue(0, 1) == getLvalue(2, 1))) && (getLvalue(1, 1) == getLvalue(1, 1))) && (getLvalue(1, 1) == getLvalue(2, 1))) && (getLvalue(2, 1) == getLvalue(2, 1)))
    {
      return 0;
    }

  }

}


