#define BOUND 0
#define DISABLELSTIG 0
#define MAXCOMPONENTS 3
#define MAXKEYE 1
#define MAXKEYI 2
#define MAXKEYL 6
#define MAXPC 1
#define MAXTUPLE 4
#define undef_value -128 // SHRT_MIN

#define LABSassume(COND)            __VERIFIER_assume(COND)   

#ifdef SIMULATION
    #define LABScheck(pcs, guards)      ((pcs) & (guards))
    #define LABSassert(COND, LABEL)     if(!(COND)) { printf(">>>" #LABEL " violated"); } else { printf(">>>" #LABEL " satisfied"); } 
#else 
    #define LABScheck(pcs, guards)      (pcs)
    #define LABSassert(COND, LABEL)     /*#LABEL*/ assert(COND)
#endif

typedef short TYPEOFVALUES;
typedef unsigned char TYPEOFPC;
typedef unsigned char TYPEOFTIME;
typedef unsigned char TYPEOFAGENTID;
typedef unsigned char TYPEOFKEYIID;
typedef unsigned char TYPEOFKEYLID;
typedef unsigned char TYPEOFKEYEID;
typedef unsigned char Bool;


TYPEOFVALUES __abs(TYPEOFVALUES x) {
  return (x>0) ? x : -x;
}

TYPEOFVALUES __max(TYPEOFVALUES x, TYPEOFVALUES y) { return (x > y) ? x : y; }
TYPEOFVALUES __min(TYPEOFVALUES x, TYPEOFVALUES y) { return (x < y) ? x : y; }

TYPEOFVALUES mod(TYPEOFVALUES n, TYPEOFVALUES m) {
  return n >= 0 ? n % m : m + (n % m);
}

TYPEOFVALUES I[MAXCOMPONENTS][MAXKEYI];
TYPEOFVALUES E[MAXKEYE];
//Bool terminated[MAXCOMPONENTS];

TYPEOFPC pc[MAXCOMPONENTS][MAXPC];

#if DISABLELSTIG == 0

TYPEOFTIME __LABS_time;
Bool Hin[MAXCOMPONENTS][MAXKEYL];
Bool Hout[MAXCOMPONENTS][MAXKEYL]; 
unsigned char HinCnt[MAXCOMPONENTS];
unsigned char HoutCnt[MAXCOMPONENTS];

TYPEOFTIME now(void) {
    //assert((TYPEOFTIME) (__LABS_time+1) > (TYPEOFTIME)__LABS_time);
    return ++__LABS_time;
}

TYPEOFVALUES Lvalue[MAXCOMPONENTS][MAXKEYL];
TYPEOFTIME Ltstamp[MAXCOMPONENTS][MAXKEYL];

const TYPEOFKEYLID tupleStart[MAXKEYL] = { 0, 0, 0, 0, 4, 4 };
const TYPEOFKEYLID tupleEnd[MAXKEYL] = { 3, 3, 3, 3, 5, 5 };

Bool link(TYPEOFAGENTID __LABS_link1, TYPEOFAGENTID __LABS_link2, TYPEOFKEYLID key) {
    Bool __LABS_link = 0;
    if ((key >= 4) & (key <= 5)){
        __LABS_link = (((((I[__LABS_link1][0]) - (I[__LABS_link2][0])) * ((I[__LABS_link1][0]) - (I[__LABS_link2][0]))) + (((I[__LABS_link1][1]) - (I[__LABS_link2][1])) * ((I[__LABS_link1][1]) - (I[__LABS_link2][1])))) <= ((5) * (5)));
    }
    else if ((key >= 0) & (key <= 3)){
        __LABS_link = ((Lvalue[__LABS_link1][0]) >= (Lvalue[__LABS_link2][0]));
    }

    return __LABS_link;
}

TYPEOFTIME timeof(TYPEOFAGENTID id, TYPEOFKEYLID key) {
    return Ltstamp[id][tupleStart[key]];
}

void setHin(TYPEOFAGENTID id, TYPEOFKEYLID key) {
    // if (Hin[id][tupleStart[key]] == 0) {
    //     Hin[id][tupleStart[key]] = 1;
    //     HinCnt[id] = HinCnt[id] + 1;
    // }
    HinCnt[id] = HinCnt[id] + (!Hin[id][tupleStart[key]]);
    Hin[id][tupleStart[key]] = 1;
}

void clearHin(TYPEOFAGENTID id, TYPEOFKEYLID key) {
    // if (Hin[id][tupleStart[key]] == 1) {
    //     Hin[id][tupleStart[key]] = 0;
    //     HinCnt[id] = HinCnt[id] - 1;
    // }
    HinCnt[id] = HinCnt[id] - (Hin[id][tupleStart[key]]);
    // assert(HinCnt[id] >= 0);
    Hin[id][tupleStart[key]] = 0;
}

void setHout(TYPEOFAGENTID id, TYPEOFKEYLID key) {
    // if (Hout[id][tupleStart[key]] == 0) {
    //     Hout[id][tupleStart[key]] = 1;
    //     HoutCnt[id] = HoutCnt[id] + 1;
    // }
    HoutCnt[id] = HoutCnt[id] + (!Hout[id][tupleStart[key]]);
    Hout[id][tupleStart[key]] = 1;
}

void clearHout(TYPEOFAGENTID id, TYPEOFKEYLID key) {
    // if (Hout[id][tupleStart[key]] == 1) {
    //     Hout[id][tupleStart[key]] = 0;
    //     HoutCnt[id] = HoutCnt[id] - 1;
    // }
    // assert(HoutCnt[id] > 0);
    HoutCnt[id] = HoutCnt[id] - (Hout[id][tupleStart[key]]);
    Hout[id][tupleStart[key]] = 0;
}
#endif

//
//  Rule ATTR
//  Component component_id  assigns to key the evaluated expression
//  If check is true, transition is guarded by HoutCnt == HinCnt == 0
//
void attr(TYPEOFAGENTID id, TYPEOFKEYIID key, TYPEOFVALUES value, Bool check) {
    #if DISABLELSTIG == 0
    __VERIFIER_assume((!check) | (HoutCnt[id] == 0));
    __VERIFIER_assume((!check) | (HinCnt[id] == 0));
    #endif

    I[id][key] = value;
    #if DISABLELSTIG == 0
    now(); // local step
    #endif
}

void env(TYPEOFAGENTID id, TYPEOFKEYEID key, TYPEOFVALUES value, Bool check) {
    #if DISABLELSTIG == 0
    __VERIFIER_assume((!check) | (HoutCnt[id] == 0));
    __VERIFIER_assume((!check) | (HinCnt[id] == 0));
    #endif

    E[key] = value;
    #if DISABLELSTIG == 0
    now(); // local step
    #endif
}

#if DISABLELSTIG == 0
//
//  Rule LSTIG
//
void lstig(TYPEOFAGENTID id, TYPEOFKEYLID key, TYPEOFVALUES value, Bool check) {
    __VERIFIER_assume((!check) | (HoutCnt[id] == 0));
    __VERIFIER_assume((!check) | (HinCnt[id] == 0));

    Lvalue[id][key] = value;
    // Only update the timestamp of the 1st element in the tuple
    Ltstamp[id][tupleStart[key]] = now();
    
    setHout(id, key);
}

Bool differentLstig(TYPEOFAGENTID comp1, TYPEOFAGENTID comp2, TYPEOFKEYLID key) {
    TYPEOFKEYLID k  = tupleStart[key];
    return ((Lvalue[comp1][k] != Lvalue[comp1][k]) | (Ltstamp[comp1][k] != Ltstamp[comp2][k]));
}

void confirm(void) {
    TYPEOFAGENTID guessedcomp;
    __VERIFIER_assume(guessedcomp < MAXCOMPONENTS);
    __VERIFIER_assume(HinCnt[guessedcomp] > 0);

    TYPEOFKEYLID guessedkey;
    __VERIFIER_assume(guessedkey < MAXKEYL);
    __VERIFIER_assume(Hin[guessedcomp][guessedkey] == 1);

    // NOTE: Since SetHin(), SetHout() only work on tupleStarts,
    // guessedkey is guaranteed to be the 1st element of some tuple

    // assert(guessedkey == tupleStart[guessedkey]);

    TYPEOFAGENTID i;
    TYPEOFTIME t = timeof(guessedcomp, guessedkey);
    
    // Send data from guessedcomp to i
    for (i=0; i<MAXCOMPONENTS; i++) {
        if (((guessedcomp!=i) & (timeof(i, guessedkey) != t)) && 
            link(guessedcomp,i,guessedkey)) {
            
            setHout(i, guessedkey);
            // If data is fresh, agent i copies it to its stigmergy
            if (timeof(i, guessedkey) < t) {
                TYPEOFKEYLID k, next;
                clearHin(i, guessedkey);
                for (k = 0; k < MAXTUPLE; k++) {
                    next = guessedkey + k;
                    // if ((next<MAXKEYL) && (tupleStart[next] == guessedkey))
                    if (next <= tupleEnd[guessedkey])
                        Lvalue[i][next] = Lvalue[guessedcomp][next];
                }
                Ltstamp[i][guessedkey] = t;
            }
        }
    }
    clearHin(guessedcomp, guessedkey);
}

void propagate(void) {
    TYPEOFAGENTID guessedcomp;
    __VERIFIER_assume(guessedcomp < MAXCOMPONENTS);
    __VERIFIER_assume(HoutCnt[guessedcomp] > 0);

    TYPEOFKEYLID guessedkey;
    __VERIFIER_assume(guessedkey < MAXKEYL);
    __VERIFIER_assume(Hout[guessedcomp][guessedkey] == 1);

    // assert(guessedkey == tupleStart[guessedkey]);

    TYPEOFAGENTID i;
    TYPEOFTIME t = timeof(guessedcomp, guessedkey);

    for (i=0; i<MAXCOMPONENTS; i++) {
        if (((guessedcomp!=i) & (timeof(i, guessedkey)<t)) && (link(guessedcomp,i,guessedkey))) {
            // If data is fresh, i copies it to its stigmergy and
            // will propagate it in the future (setHout)
            setHout(i, guessedkey);
            clearHin(i, guessedkey);
            TYPEOFKEYLID k, next;
            for (k = 0; k < MAXTUPLE; k++) {
                next = guessedkey+k;
                // if (next<MAXKEYL && tupleStart[next] == tupleStart[guessedkey])
                if (next <= tupleEnd[guessedkey])
                    Lvalue[i][next] = Lvalue[guessedcomp][next];
            }
            Ltstamp[i][guessedkey] = t;

        }
    }
    clearHout(guessedcomp, guessedkey);
}
#endif

void init() {

    TYPEOFVALUES _I[MAXCOMPONENTS][MAXKEYI];
    TYPEOFVALUES _E[MAXKEYE];
    TYPEOFPC _pc[MAXCOMPONENTS][MAXPC];
    #if DISABLELSTIG == 0
    TYPEOFVALUES _Lvalue[MAXCOMPONENTS][MAXKEYL];
    #endif

    unsigned char i, j;
    for (i=0; i<MAXCOMPONENTS; i++) {
        //terminated[i] = 0;
      for (j = 0; j < MAXKEYI; j++) {
        _I[i][j] = __VERIFIER_nondet_int();
      }
#if DISABLELSTIG == 0    
      for (j = 0; j < MAXKEYL; j++) {
          _Lvalue[i][j] = __VERIFIER_nondet_int();
          Ltstamp[i][j] = 0;
          Hin[i][j] = 0;
          Hout[i][j] = 0;
      }
      HinCnt[i] = 0;
      HoutCnt[i] = 0;
#endif
    }


    _pc[0][0] = 8;

    _pc[1][0] = 8;

    _pc[2][0] = 8;
        
    LABSassume((((_Lvalue[0][0]) == (1))));
    LABSassume((((_Lvalue[1][0]) == (1))));
    LABSassume((((_Lvalue[2][0]) == (1))));
    LABSassume((((_Lvalue[0][1]) == (0))));
    LABSassume((((_Lvalue[1][1]) == (1))));
    LABSassume((((_Lvalue[2][1]) == (2))));
    LABSassume((((_Lvalue[0][2]) == (-(1)))));
    LABSassume((((_Lvalue[1][2]) == (-(1)))));
    LABSassume((((_Lvalue[2][2]) == (-(1)))));
    LABSassume((((_Lvalue[0][3]) == (-(1)))));
    LABSassume((((_Lvalue[1][3]) == (-(1)))));
    LABSassume((((_Lvalue[2][3]) == (-(1)))));
    LABSassume((((_Lvalue[0][4]) == (-(1)))) | (((_Lvalue[0][4]) == (1))));
    LABSassume((((_Lvalue[1][4]) == (-(1)))) | (((_Lvalue[1][4]) == (1))));
    LABSassume((((_Lvalue[2][4]) == (-(1)))) | (((_Lvalue[2][4]) == (1))));
    LABSassume((((_Lvalue[0][5]) == (-(1)))) | (((_Lvalue[0][5]) == (1))));
    LABSassume((((_Lvalue[1][5]) == (-(1)))) | (((_Lvalue[1][5]) == (1))));
    LABSassume((((_Lvalue[2][5]) == (-(1)))) | (((_Lvalue[2][5]) == (1))));
    LABSassume((((_I[0][0]) >= (0))) & (((_I[0][0]) < (5))));
    LABSassume((((_I[1][0]) >= (0))) & (((_I[1][0]) < (5))));
    LABSassume((((_I[2][0]) >= (0))) & (((_I[2][0]) < (5))));
    LABSassume((((_I[0][1]) >= (0))) & (((_I[0][1]) < (5))));
    LABSassume((((_I[1][1]) >= (0))) & (((_I[1][1]) < (5))));
    LABSassume((((_I[2][1]) >= (0))) & (((_I[2][1]) < (5))));
#if DISABLELSTIG == 0
    Ltstamp[0][tupleStart[0]] = now();
    Ltstamp[1][tupleStart[0]] = now();
    Ltstamp[2][tupleStart[0]] = now();
    Ltstamp[0][tupleStart[1]] = now();
    Ltstamp[1][tupleStart[1]] = now();
    Ltstamp[2][tupleStart[1]] = now();
    Ltstamp[0][tupleStart[2]] = now();
    Ltstamp[1][tupleStart[2]] = now();
    Ltstamp[2][tupleStart[2]] = now();
    Ltstamp[0][tupleStart[3]] = now();
    Ltstamp[1][tupleStart[3]] = now();
    Ltstamp[2][tupleStart[3]] = now();
    Ltstamp[0][tupleStart[4]] = now();
    Ltstamp[1][tupleStart[4]] = now();
    Ltstamp[2][tupleStart[4]] = now();
    Ltstamp[0][tupleStart[5]] = now();
    Ltstamp[1][tupleStart[5]] = now();
    Ltstamp[2][tupleStart[5]] = now();
    now();
#endif


    for (i=0; i<MAXKEYE; i++) {
        E[i] = _E[i];
    }
    for (i=0; i<MAXCOMPONENTS; i++) {
        for (j=0; j<MAXPC; j++) {
            pc[i][j] = _pc[i][j];
        }

        for (j=0; j<MAXKEYI; j++) {
            I[i][j] = _I[i][j];
        }
#if DISABLELSTIG == 0
        for (j=0; j<MAXKEYL; j++) {
            Lvalue[i][j] = _Lvalue[i][j];
        }
#endif
    }
}

void _0_2(int tid) {
    //((leader, 1)) == (id)->(posX, 2),(posY, 3) <~ (x, 0),(y, 1)
    


    LABSassume(((Lvalue[tid][1]) == (tid)));

    TYPEOFVALUES val0 = I[tid][0];
    TYPEOFVALUES val1 = I[tid][1];

    lstig(tid, 2, val0, 1);
    lstig(tid, 3, val1, 0);
    setHin(tid, 1);


    pc[tid][0] = 8;

}

void _0_3(int tid) {
    //(abs((y, 1) - (posY, 3))) > (5)->(dirY, 5) <~ (posY, 3) - (y, 1) / abs((posY, 3) - (y, 1))
    


    LABSassume(((__abs((I[tid][1]) - (Lvalue[tid][3]))) > (5)));

    TYPEOFVALUES val0 = ((Lvalue[tid][3]) - (I[tid][1])) / (__abs((Lvalue[tid][3]) - (I[tid][1])));

    lstig(tid, 5, val0, 1);
    setHin(tid, 3);


    pc[tid][0] = 8;

}

void _0_4(int tid) {
    //(abs((y, 1) - (posY, 3))) <= (5)->√
    

    LABSassume((HoutCnt[tid] == 0) & (HinCnt[tid] == 0));

    LABSassume(((__abs((I[tid][1]) - (Lvalue[tid][3]))) <= (5)));


    setHin(tid, 3);


    pc[tid][0] = 8;

}

void _0_5(int tid) {
    //(abs((x, 0) - (posX, 2))) > (5)->(dirX, 4) <~ (posX, 2) - (x, 0) / abs((posX, 2) - (x, 0))
    


    LABSassume(((__abs((I[tid][0]) - (Lvalue[tid][2]))) > (5)));

    TYPEOFVALUES val0 = ((Lvalue[tid][2]) - (I[tid][0])) / (__abs((Lvalue[tid][2]) - (I[tid][0])));

    lstig(tid, 4, val0, 1);
    setHin(tid, 2);


    TYPEOFPC pc0;
    LABSassume((pc0 == 3) | (pc0 == 4));
    pc[tid][0] = pc0;

}

void _0_6(int tid) {
    //(abs((x, 0) - (posX, 2))) <= (5)->√
    

    LABSassume((HoutCnt[tid] == 0) & (HinCnt[tid] == 0));

    LABSassume(((__abs((I[tid][0]) - (Lvalue[tid][2]))) <= (5)));


    setHin(tid, 2);


    TYPEOFPC pc0;
    LABSassume((pc0 == 3) | (pc0 == 4));
    pc[tid][0] = pc0;

}

void _0_7(int tid) {
    //((leader, 1)) != (id)->(count, 0) <~ (count, 0) + 1
    


    LABSassume(((Lvalue[tid][1]) != (tid)));

    TYPEOFVALUES val0 = (Lvalue[tid][0]) + (1);

    lstig(tid, 0, val0, 1);
    setHin(tid, 0);
    setHin(tid, 1);


    TYPEOFPC pc0;
    LABSassume((pc0 == 5) | (pc0 == 6));
    pc[tid][0] = pc0;

}

void _0_8(int tid) {
    //(x, 0),(y, 1) <- (x, 0) + (dirX, 4) % 5,(y, 1) + (dirY, 5) % 5
    


    TYPEOFVALUES val0 = mod((I[tid][0]) + (Lvalue[tid][4]), 5);
    TYPEOFVALUES val1 = mod((I[tid][1]) + (Lvalue[tid][5]), 5);

    attr(tid, 0, val0, 1);
    attr(tid, 1, val1, 0);
    setHin(tid, 4);
    setHin(tid, 5);


    TYPEOFPC pc0;
    LABSassume((pc0 == 2) | (pc0 == 7));
    pc[tid][0] = pc0;

}

void monitor() {
}

void finally() {
    LABSassert((((Lvalue[0][1]) == (Lvalue[0][1]))) & (((Lvalue[0][1]) == (Lvalue[1][1]))) & (((Lvalue[0][1]) == (Lvalue[2][1]))) & (((Lvalue[1][1]) == (Lvalue[1][1]))) & (((Lvalue[1][1]) == (Lvalue[2][1]))) & (((Lvalue[2][1]) == (Lvalue[2][1]))), OneLeader);
    #ifdef SIMULATION
    assert(0);
    #endif
}

int main(void) {
    init();
    TYPEOFAGENTID firstAgent = 0;

    #if DISABLELSTIG == 0
        #if BOUND > 0
    Bool sys_or_not[BOUND];
        #endif
    #endif

    #if BOUND > 0
    unsigned char switchnondet[BOUND];
    unsigned __LABS_step;
    for (__LABS_step=0; __LABS_step<BOUND; __LABS_step++) {
    #else
    while(1) {        
    #endif
        // if (terminalState()) break;
        
        #if DISABLELSTIG == 0
            #if BOUND > 0
        if (sys_or_not[__LABS_step]) {
            #else
        if ((Bool) __VERIFIER_nondet()) {
            #endif
        #endif
            //LABSassume(firstAgent < MAXCOMPONENTS);

            #if BOUND > 0
            switch (switchnondet[__LABS_step]) {
            #else
            switch (pc[firstAgent][0]) {
            #endif

                case 2: _0_2(firstAgent); break;
                case 3: _0_3(firstAgent); break;
                case 4: _0_4(firstAgent); break;
                case 5: _0_5(firstAgent); break;
                case 6: _0_6(firstAgent); break;
                case 7: _0_7(firstAgent); break;
                case 8: _0_8(firstAgent); break;
              default: {}//LABSassume(0);
            }
            
            if (firstAgent == MAXCOMPONENTS - 1) {
                firstAgent = 0;
            }
            else {
                firstAgent++;
            }
        #if DISABLELSTIG == 0 
        }
        else {
            Bool propagate_or_confirm; 

            if (propagate_or_confirm) propagate();
            else confirm();
        }
        #endif
        //monitor();
        
        if ((((Lvalue[0][1]) == (Lvalue[0][1]))) & (((Lvalue[0][1]) == (Lvalue[1][1]))) & (((Lvalue[0][1]) == (Lvalue[2][1]))) & (((Lvalue[1][1]) == (Lvalue[1][1]))) & (((Lvalue[1][1]) == (Lvalue[2][1]))) & (((Lvalue[2][1]) == (Lvalue[2][1])))) { return 0; }
    }
    
    //finally();
}



