#define BOUND 0
#define DISABLELSTIG 0
#define MAXCOMPONENTS 3
#define MAXKEYE 1
#define MAXKEYI 1
#define MAXKEYL 2
#define MAXPC 1
#define MAXTUPLE 1
#define undef_value -128 // SHRT_MIN

#define LABSassume(COND)            __VERIFIER_assume(COND)   

#ifdef SIMULATION
    #define LABScheck(pcs, guards)      ((pcs) & (guards))
    #define LABSassert(COND, LABEL)     if(!(COND)) { printf(">>>" #LABEL " violated"); } else { printf(">>>" #LABEL " satisfied"); } 
#else 
    #define LABScheck(pcs, guards)      (pcs)
    #define LABSassert(COND, LABEL)     /*#LABEL*/ assert(COND)
#endif

typedef short TYPEOFVALUES;
typedef unsigned char TYPEOFPC;
typedef unsigned char TYPEOFTIME;
typedef unsigned char TYPEOFAGENTID;
typedef unsigned char TYPEOFKEYIID;
typedef unsigned char TYPEOFKEYLID;
typedef unsigned char TYPEOFKEYEID;
typedef unsigned char Bool;


TYPEOFVALUES __abs(TYPEOFVALUES x) {
  return (x>0) ? x : -x;
}

TYPEOFVALUES __max(TYPEOFVALUES x, TYPEOFVALUES y) { return (x > y) ? x : y; }
TYPEOFVALUES __min(TYPEOFVALUES x, TYPEOFVALUES y) { return (x < y) ? x : y; }

TYPEOFVALUES mod(TYPEOFVALUES n, TYPEOFVALUES m) {
  return n >= 0 ? n % m : m + (n % m);
}

TYPEOFVALUES I[MAXCOMPONENTS][MAXKEYI];
TYPEOFVALUES E[MAXKEYE];
Bool terminated[MAXCOMPONENTS];

TYPEOFPC pc[MAXCOMPONENTS][MAXPC];

#if DISABLELSTIG == 0

TYPEOFTIME __LABS_time;
Bool Hin[MAXCOMPONENTS][MAXKEYL];
Bool Hout[MAXCOMPONENTS][MAXKEYL]; 
unsigned char HinCnt[MAXCOMPONENTS];
unsigned char HoutCnt[MAXCOMPONENTS];

TYPEOFTIME now(void) {
    assert((TYPEOFTIME) (__LABS_time+1) > (TYPEOFTIME)__LABS_time);
    return ++__LABS_time;
}

TYPEOFVALUES Lvalue[MAXCOMPONENTS][MAXKEYL];
TYPEOFTIME Ltstamp[MAXCOMPONENTS][MAXKEYL];

const TYPEOFKEYLID tupleStart[MAXKEYL] = { 0, 1 };
const TYPEOFKEYLID tupleEnd[MAXKEYL] = { 0, 1 };

Bool link(TYPEOFAGENTID __LABS_link1, TYPEOFAGENTID __LABS_link2, TYPEOFKEYLID key) {
    Bool __LABS_link = 0;
    if ((key >= 0) & (key <= 0)){
        __LABS_link = ((((I[__LABS_link1][0]) - (I[__LABS_link2][0])) >= (0))) & ((((I[__LABS_link1][0]) - (I[__LABS_link2][0])) <= (2)));
    }
    else if ((key >= 1) & (key <= 1)){
        __LABS_link = ((((I[__LABS_link2][0]) - (I[__LABS_link1][0])) > (0))) & ((((I[__LABS_link2][0]) - (I[__LABS_link1][0])) <= (2)));
    }

    return __LABS_link;
}

TYPEOFTIME timeof(TYPEOFAGENTID id, TYPEOFKEYLID key) {
    return Ltstamp[id][tupleStart[key]];
}

void setHin(TYPEOFAGENTID id, TYPEOFKEYLID key) {
    // if (Hin[id][tupleStart[key]] == 0) {
    //     Hin[id][tupleStart[key]] = 1;
    //     HinCnt[id] = HinCnt[id] + 1;
    // }
    HinCnt[id] = HinCnt[id] + (!Hin[id][tupleStart[key]]);
    Hin[id][tupleStart[key]] = 1;
}

void clearHin(TYPEOFAGENTID id, TYPEOFKEYLID key) {
    // if (Hin[id][tupleStart[key]] == 1) {
    //     Hin[id][tupleStart[key]] = 0;
    //     HinCnt[id] = HinCnt[id] - 1;
    // }
    HinCnt[id] = HinCnt[id] - (Hin[id][tupleStart[key]]);
    // assert(HinCnt[id] >= 0);
    Hin[id][tupleStart[key]] = 0;
}

void setHout(TYPEOFAGENTID id, TYPEOFKEYLID key) {
    // if (Hout[id][tupleStart[key]] == 0) {
    //     Hout[id][tupleStart[key]] = 1;
    //     HoutCnt[id] = HoutCnt[id] + 1;
    // }
    HoutCnt[id] = HoutCnt[id] + (!Hout[id][tupleStart[key]]);
    Hout[id][tupleStart[key]] = 1;
}

void clearHout(TYPEOFAGENTID id, TYPEOFKEYLID key) {
    // if (Hout[id][tupleStart[key]] == 1) {
    //     Hout[id][tupleStart[key]] = 0;
    //     HoutCnt[id] = HoutCnt[id] - 1;
    // }
    // assert(HoutCnt[id] > 0);
    HoutCnt[id] = HoutCnt[id] - (Hout[id][tupleStart[key]]);
    Hout[id][tupleStart[key]] = 0;
}
#endif

//
//  Rule ATTR
//  Component component_id  assigns to key the evaluated expression
//  If check is true, transition is guarded by HoutCnt == HinCnt == 0
//
void attr(TYPEOFAGENTID id, TYPEOFKEYIID key, TYPEOFVALUES value, Bool check) {
    #if DISABLELSTIG == 0
    __VERIFIER_assume((!check) | (HoutCnt[id] == 0));
    __VERIFIER_assume((!check) | (HinCnt[id] == 0));
    #endif

    I[id][key] = value;
    #if DISABLELSTIG == 0
    now(); // local step
    #endif
}

void env(TYPEOFAGENTID id, TYPEOFKEYEID key, TYPEOFVALUES value, Bool check) {
    #if DISABLELSTIG == 0
    __VERIFIER_assume((!check) | (HoutCnt[id] == 0));
    __VERIFIER_assume((!check) | (HinCnt[id] == 0));
    #endif

    E[key] = value;
    #if DISABLELSTIG == 0
    now(); // local step
    #endif
}

#if DISABLELSTIG == 0
//
//  Rule LSTIG
//
void lstig(TYPEOFAGENTID id, TYPEOFKEYLID key, TYPEOFVALUES value, Bool check) {
    __VERIFIER_assume((!check) | (HoutCnt[id] == 0));
    __VERIFIER_assume((!check) | (HinCnt[id] == 0));

    Lvalue[id][key] = value;
    // Only update the timestamp of the 1st element in the tuple
    Ltstamp[id][tupleStart[key]] = now();
    
    setHout(id, key);
}

Bool differentLstig(TYPEOFAGENTID comp1, TYPEOFAGENTID comp2, TYPEOFKEYLID key) {
    TYPEOFKEYLID k  = tupleStart[key];
    return ((Lvalue[comp1][k] != Lvalue[comp1][k]) | (Ltstamp[comp1][k] != Ltstamp[comp2][k]));
}

void confirm(void) {
    TYPEOFAGENTID guessedcomp;
    __VERIFIER_assume(guessedcomp < MAXCOMPONENTS);
    __VERIFIER_assume(HinCnt[guessedcomp] > 0);

    TYPEOFKEYLID guessedkey;
    __VERIFIER_assume(guessedkey < MAXKEYL);
    __VERIFIER_assume(Hin[guessedcomp][guessedkey] == 1);

    // NOTE: Since SetHin(), SetHout() only work on tupleStarts,
    // guessedkey is guaranteed to be the 1st element of some tuple

    // assert(guessedkey == tupleStart[guessedkey]);

    TYPEOFAGENTID i;
    TYPEOFTIME t = timeof(guessedcomp, guessedkey);
    
    // Send data from guessedcomp to i
    for (i=0; i<MAXCOMPONENTS; i++) {
        if (((guessedcomp!=i) & (timeof(i, guessedkey) != t)) && 
            link(guessedcomp,i,guessedkey)) {
            
            setHout(i, guessedkey);
            // If data is fresh, agent i copies it to its stigmergy
            if (timeof(i, guessedkey) < t) {
                TYPEOFKEYLID k, next;
                clearHin(i, guessedkey);
                for (k = 0; k < MAXTUPLE; k++) {
                    next = guessedkey + k;
                    // if ((next<MAXKEYL) && (tupleStart[next] == guessedkey))
                    if (next <= tupleEnd[guessedkey])
                        Lvalue[i][next] = Lvalue[guessedcomp][next];
                }
                Ltstamp[i][guessedkey] = t;
            }
        }
    }
    clearHin(guessedcomp, guessedkey);
}

void propagate(void) {
    TYPEOFAGENTID guessedcomp;
    __VERIFIER_assume(guessedcomp < MAXCOMPONENTS);
    __VERIFIER_assume(HoutCnt[guessedcomp] > 0);

    TYPEOFKEYLID guessedkey;
    __VERIFIER_assume(guessedkey < MAXKEYL);
    __VERIFIER_assume(Hout[guessedcomp][guessedkey] == 1);

    // assert(guessedkey == tupleStart[guessedkey]);

    TYPEOFAGENTID i;
    TYPEOFTIME t = timeof(guessedcomp, guessedkey);

    for (i=0; i<MAXCOMPONENTS; i++) {
        if (((guessedcomp!=i) & (timeof(i, guessedkey)<t)) && (link(guessedcomp,i,guessedkey))) {
            // If data is fresh, i copies it to its stigmergy and
            // will propagate it in the future (setHout)
            setHout(i, guessedkey);
            clearHin(i, guessedkey);
            TYPEOFKEYLID k, next;
            for (k = 0; k < MAXTUPLE; k++) {
                next = guessedkey+k;
                // if (next<MAXKEYL && tupleStart[next] == tupleStart[guessedkey])
                if (next <= tupleEnd[guessedkey])
                    Lvalue[i][next] = Lvalue[guessedcomp][next];
            }
            Ltstamp[i][guessedkey] = t;

        }
    }
    clearHout(guessedcomp, guessedkey);
}
#endif

void init() {

    TYPEOFVALUES _I[MAXCOMPONENTS][MAXKEYI];
    TYPEOFVALUES _E[MAXKEYE];
    TYPEOFPC _pc[MAXCOMPONENTS][MAXPC];
    #if DISABLELSTIG == 0
    TYPEOFVALUES _Lvalue[MAXCOMPONENTS][MAXKEYL];
    #endif

    unsigned char i, j;
    for (i=0; i<MAXCOMPONENTS; i++) {
        terminated[i] = 0;
#if DISABLELSTIG == 0    
        for (j=0; j<MAXKEYL; j++) {
            Ltstamp[i][j] = 0;
            Hin[i][j] = 0;
            Hout[i][j] = 0;
        }
        HinCnt[i] = 0;
        HoutCnt[i] = 0;
#endif
    }


    _pc[0][0] = 6;

    _pc[1][0] = 6;

    _pc[2][0] = 6;
        
    LABSassume(((_Lvalue[0][0]) == (undef_value)));
    LABSassume(((_Lvalue[1][0]) == (undef_value)));
    LABSassume(((_Lvalue[2][0]) == (undef_value)));
    LABSassume(((_Lvalue[0][1]) == (undef_value)));
    LABSassume(((_Lvalue[1][1]) == (undef_value)));
    LABSassume(((_Lvalue[2][1]) == (undef_value)));
    LABSassume((((_I[0][0]) >= (1))) & (((_I[0][0]) < (10))));
    LABSassume((((_I[1][0]) >= (1))) & (((_I[1][0]) < (10))));
    LABSassume((((_I[2][0]) >= (1))) & (((_I[2][0]) < (10))));
#if DISABLELSTIG == 0
    Ltstamp[0][tupleStart[0]] = now();
    Ltstamp[1][tupleStart[0]] = now();
    Ltstamp[2][tupleStart[0]] = now();
    Ltstamp[0][tupleStart[1]] = now();
    Ltstamp[1][tupleStart[1]] = now();
    Ltstamp[2][tupleStart[1]] = now();
    now();
#endif


    for (i=0; i<MAXKEYE; i++) {
        E[i] = _E[i];
    }
    for (i=0; i<MAXCOMPONENTS; i++) {
        for (j=0; j<MAXPC; j++) {
            pc[i][j] = _pc[i][j];
        }

        for (j=0; j<MAXKEYI; j++) {
            I[i][j] = _I[i][j];
        }
#if DISABLELSTIG == 0
        for (j=0; j<MAXKEYL; j++) {
            Lvalue[i][j] = _Lvalue[i][j];
        }
#endif
    }
}

void _0_2(int tid) {
    //((idLeft, 0)) != (id) and ((pos, 0)) > (0)->(pos, 0) <- (pos, 0) - 1
    

    LABSassume((((Lvalue[tid][0]) != (tid))) & (((Lvalue[tid][0]) != (undef_value))));

    LABSassume(((I[tid][0]) > (0)));

    TYPEOFVALUES val0 = (I[tid][0]) - (1);

    attr(tid, 0, val0, 1);
    setHin(tid, 0);


    pc[tid][0] = 6;

}

void _0_3(int tid) {
    //((idLeft, 0)) != (id) and ((pos, 0)) == (0)->(pos, 0) <- (pos, 0) + 1
    

    LABSassume((((Lvalue[tid][0]) != (tid))) & (((Lvalue[tid][0]) != (undef_value))));

    LABSassume(((I[tid][0]) == (0)));

    TYPEOFVALUES val0 = (I[tid][0]) + (1);

    attr(tid, 0, val0, 1);
    setHin(tid, 0);


    pc[tid][0] = 6;

}

void _0_4(int tid) {
    //((idRight, 1)) != (id) and ((pos, 0)) < (10 - 1)->(pos, 0) <- (pos, 0) + 1
    

    LABSassume((((Lvalue[tid][1]) != (tid))) & (((Lvalue[tid][1]) != (undef_value))));

    LABSassume(((I[tid][0]) < ((10) - (1))));

    TYPEOFVALUES val0 = (I[tid][0]) + (1);

    attr(tid, 0, val0, 1);
    setHin(tid, 1);


    pc[tid][0] = 6;

}

void _0_5(int tid) {
    //((idRight, 1)) != (id) and ((pos, 0)) == (10 - 1)->(pos, 0) <- (pos, 0) - 1
    

    LABSassume((((Lvalue[tid][1]) != (tid))) & (((Lvalue[tid][1]) != (undef_value))));

    LABSassume(((I[tid][0]) == ((10) - (1))));

    TYPEOFVALUES val0 = (I[tid][0]) - (1);

    attr(tid, 0, val0, 1);
    setHin(tid, 1);


    pc[tid][0] = 6;

}

void _0_6(int tid) {
    //(idLeft, 0),(idRight, 1) <~ id,id
    

    TYPEOFVALUES val0 = tid;
    TYPEOFVALUES val1 = tid;

    lstig(tid, 0, val0, 1);
    lstig(tid, 1, val1, 0);


    TYPEOFPC pc0;
    LABSassume((pc0 == 2) | (pc0 == 3) | (pc0 == 4) | (pc0 == 5));
    pc[tid][0] = pc0;

}

void monitor() {
}

/*void finally() {
    LABSassert(((((0) == (1))) | (((__abs((I[0][0]) - (I[1][0]))) >= (2)))) & ((((0) == (2))) | (((__abs((I[0][0]) - (I[2][0]))) >= (2)))) & ((((1) == (0))) | (((__abs((I[1][0]) - (I[0][0]))) >= (2)))) & ((((1) == (2))) | (((__abs((I[1][0]) - (I[2][0]))) >= (2)))) & ((((2) == (0))) | (((__abs((I[2][0]) - (I[0][0]))) >= (2)))) & ((((2) == (1))) | (((__abs((I[2][0]) - (I[1][0]))) >= (2)))), P1);
    #ifdef SIMULATION
    assert(0);
    #endif
}*/

int main(void) {
    init();
    TYPEOFAGENTID firstAgent = 0;

    #if DISABLELSTIG == 0
        #if BOUND > 0
    Bool sys_or_not[BOUND];
        #endif
    #endif

    #if BOUND > 0
    unsigned char switchnondet[BOUND];
    unsigned __LABS_step;
    for (__LABS_step=0; __LABS_step<BOUND; __LABS_step++) {
    #else
    while(1) {        
    #endif
        // if (terminalState()) break;
        
        #if DISABLELSTIG == 0
            #if BOUND > 0
        if (sys_or_not[__LABS_step]) {
            #else
        if ((Bool) __VERIFIER_nondet()) {
            #endif
        #endif
            LABSassume(firstAgent < MAXCOMPONENTS);

            #if BOUND > 0
            switch (switchnondet[__LABS_step]) {
            #else
            switch (pc[firstAgent][0]) {
            #endif

                case 2: _0_2(firstAgent); break;
                case 3: _0_3(firstAgent); break;
                case 4: _0_4(firstAgent); break;
                case 5: _0_5(firstAgent); break;
                case 6: _0_6(firstAgent); break;
              default: {} //LABSassume(0);
            }

            
            if (firstAgent == MAXCOMPONENTS - 1) {
                firstAgent = 0;
            }
            else {
                firstAgent++;
            }
        #if DISABLELSTIG == 0 
        }
        else {
            Bool propagate_or_confirm; 

            if (propagate_or_confirm) propagate();
            else confirm();
        }
        #endif
        //monitor();
        
        if (((((0) == (1))) | (((__abs((I[0][0]) - (I[1][0]))) >= (2)))) & ((((0) == (2))) | (((__abs((I[0][0]) - (I[2][0]))) >= (2)))) & ((((1) == (0))) | (((__abs((I[1][0]) - (I[0][0]))) >= (2)))) & ((((1) == (2))) | (((__abs((I[1][0]) - (I[2][0]))) >= (2)))) & ((((2) == (0))) | (((__abs((I[2][0]) - (I[0][0]))) >= (2)))) & ((((2) == (1))) | (((__abs((I[2][0]) - (I[1][0]))) >= (2))))) { return 0; }
    }
    
    //finally();
}



