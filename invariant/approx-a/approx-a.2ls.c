extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int(void);

typedef char TYPEOFVALUES;
typedef unsigned char TYPEOFPC;
typedef unsigned char TYPEOFTIME;
typedef unsigned char TYPEOFAGENTID;
typedef unsigned char TYPEOFKEYIID;
typedef unsigned char TYPEOFKEYLID;
typedef unsigned char TYPEOFKEYEID;
typedef unsigned char Bool;
TYPEOFVALUES __abs(TYPEOFVALUES x)
{
  return (x > 0) ? (x) : (-x);
}

TYPEOFVALUES __max(TYPEOFVALUES x, TYPEOFVALUES y)
{
  return (x > y) ? (x) : (y);
}

TYPEOFVALUES __min(TYPEOFVALUES x, TYPEOFVALUES y)
{
  return (x < y) ? (x) : (y);
}

TYPEOFVALUES mod(TYPEOFVALUES n, TYPEOFVALUES m)
{
  return (n >= 0) ? (n % m) : (m + (n % m));
}

TYPEOFVALUES I_0_0;
TYPEOFVALUES I_1_0;
TYPEOFVALUES I_2_0;
TYPEOFVALUES getI(int i, int j)
{
  switch (i)
  {
    case 0:
      return I_0_0;

    case 1:
      return I_1_0;

    case 2:
      return I_2_0;

    default:
    {
    }

  }

}

TYPEOFVALUES E_0;
TYPEOFVALUES E_1;
TYPEOFTIME __LABS_time;
unsigned char pc_0_0;
unsigned char pc_1_0;
unsigned char pc_2_0;
TYPEOFVALUES getpc(int i, int j)
{
  switch (i)
  {
    case 0:
      return pc_0_0;

    case 1:
      return pc_1_0;

    case 2:
      return pc_2_0;

    default:
    {
    }

  }

}

void setpc(int i, int j, unsigned char value)
{
  switch (i)
  {
    case 0:
      pc_0_0 = value;
      break;

    case 1:
      pc_1_0 = value;
      break;

    case 2:
      pc_2_0 = value;
      break;

    default:
    {
    }

  }

}

TYPEOFTIME now(void)
{
  return ++__LABS_time;
}

void attr(TYPEOFAGENTID id, TYPEOFKEYIID key, TYPEOFVALUES value, Bool check)
{
  switch (id)
  {
    case 0:
      I_0_0 = value;
      break;

    case 1:
      I_1_0 = value;
      break;

    case 2:
      I_2_0 = value;
      break;

    default:
    {
    }

  }

}

void env(TYPEOFAGENTID id, TYPEOFKEYEID key, TYPEOFVALUES value, Bool check)
{
  switch (key)
  {
    case 0:
      E_0 = value;
      break;

    case 1:
      E_1 = value;
      break;

    default:
    {
    }

  }

}

void init()
{
  pc_0_0 = __VERIFIER_nondet_int();
  __VERIFIER_assume(((((pc_0_0 == 2) || (pc_0_0 == 3)) || (pc_0_0 == 4)) || (pc_0_0 == 5)) || (pc_0_0 == 6));
  pc_1_0 = __VERIFIER_nondet_int();
  __VERIFIER_assume(((((pc_1_0 == 2) || (pc_1_0 == 3)) || (pc_1_0 == 4)) || (pc_1_0 == 5)) || (pc_1_0 == 6));
  pc_2_0 = __VERIFIER_nondet_int();
  __VERIFIER_assume(((((pc_2_0 == 2) || (pc_2_0 == 3)) || (pc_2_0 == 4)) || (pc_2_0 == 5)) || (pc_2_0 == 6));
  E_0 = -128;
  E_1 = -128;
  I_0_0 = 0;
  I_1_0 = 0;
  I_2_0 = 1;
  now();
}

void _0_2(int tid)
{
  TYPEOFVALUES val0 = tid;
  TYPEOFVALUES val1 = getI(tid, 0);
  TYPEOFPC pc0;
  __VERIFIER_assume(getI(tid, 0) != 2);
  env(tid, 0, val0, 1);
  env(tid, 1, val1, 0);
  __VERIFIER_assume(((((pc0 == 2) || (pc0 == 3)) || (pc0 == 4)) || (pc0 == 5)) || (pc0 == 6));
  setpc(tid, 0, pc0);
}

void _0_3(int tid)
{
  TYPEOFVALUES val0 = 1;
  TYPEOFPC pc0;
  __VERIFIER_assume((E_0 != tid) && (E_0 != (-128)));
  __VERIFIER_assume((E_1 != (-128)) && ((E_1 == 1) && (getI(tid, 0) == 2)));
  attr(tid, 0, val0, 1);
  __VERIFIER_assume(((((pc0 == 2) || (pc0 == 3)) || (pc0 == 4)) || (pc0 == 5)) || (pc0 == 6));
  setpc(tid, 0, pc0);
}

void _0_4(int tid)
{
  TYPEOFVALUES val0 = 2;
  TYPEOFPC pc0;
  __VERIFIER_assume((E_0 != tid) && (E_0 != (-128)));
  __VERIFIER_assume((E_1 != (-128)) && ((E_1 == 1) && (getI(tid, 0) == 0)));
  attr(tid, 0, val0, 1);
  __VERIFIER_assume(((((pc0 == 2) || (pc0 == 3)) || (pc0 == 4)) || (pc0 == 5)) || (pc0 == 6));
  setpc(tid, 0, pc0);
}

void _0_5(int tid)
{
  TYPEOFVALUES val0 = 2;
  TYPEOFPC pc0;
  __VERIFIER_assume((E_0 != tid) && (E_0 != (-128)));
  __VERIFIER_assume((E_1 != (-128)) && ((E_1 == 0) && (getI(tid, 0) == 1)));
  attr(tid, 0, val0, 1);
  __VERIFIER_assume(((((pc0 == 2) || (pc0 == 3)) || (pc0 == 4)) || (pc0 == 5)) || (pc0 == 6));
  setpc(tid, 0, pc0);
}

void _0_6(int tid)
{
  TYPEOFVALUES val0 = 0;
  TYPEOFPC pc0;
  __VERIFIER_assume((E_0 != tid) && (E_0 != (-128)));
  __VERIFIER_assume((E_1 != (-128)) && ((E_1 == 0) && (getI(tid, 0) == 2)));
  attr(tid, 0, val0, 1);
  __VERIFIER_assume(((((pc0 == 2) || (pc0 == 3)) || (pc0 == 4)) || (pc0 == 5)) || (pc0 == 6));
  setpc(tid, 0, pc0);
}

void monitor()
{
  assert(((((getI(3, 0) != 1) || (getI(0, 0) != 1)) || (getI(4, 0) != 1)) || (getI(1, 0) != 1)) || (getI(2, 0) != 1));
}

void finally()
{
}

int main(void)
{
  TYPEOFAGENTID firstAgent;
  init();
  while (1)
  {
    TYPEOFAGENTID newAgent;
    __VERIFIER_assume(newAgent < 3);
    firstAgent = newAgent;
    switch (getpc(firstAgent, 0))
    {
      case 2:
        _0_2(firstAgent);
        break;

      case 3:
        _0_3(firstAgent);
        break;

      case 4:
        _0_4(firstAgent);
        break;

      case 5:
        _0_5(firstAgent);
        break;

      case 6:
        _0_6(firstAgent);
        break;

      default:
      {
      }

    }

    monitor();
  }

  finally();
}


