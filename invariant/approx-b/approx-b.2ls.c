extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int(void);

typedef char TYPEOFVALUES;
typedef unsigned char TYPEOFPC;
typedef unsigned char TYPEOFTIME;
typedef unsigned char TYPEOFAGENTID;
typedef unsigned char TYPEOFKEYIID;
typedef unsigned char TYPEOFKEYLID;
typedef unsigned char TYPEOFKEYEID;
typedef unsigned char Bool;
TYPEOFVALUES __abs(TYPEOFVALUES x)
{
  return (x > 0) ? (x) : (-x);
}

TYPEOFVALUES __max(TYPEOFVALUES x, TYPEOFVALUES y)
{
  return (x > y) ? (x) : (y);
}

TYPEOFVALUES __min(TYPEOFVALUES x, TYPEOFVALUES y)
{
  return (x < y) ? (x) : (y);
}

TYPEOFVALUES mod(TYPEOFVALUES n, TYPEOFVALUES m)
{
  return (n >= 0) ? (n % m) : (m + (n % m));
}

TYPEOFVALUES I[5][1];
TYPEOFVALUES E[2];
Bool terminated[5];
TYPEOFTIME __LABS_time;
TYPEOFPC pc[5][1];
Bool Hin[5][1];
Bool Hout[5][1];
unsigned char HinCnt[5];
unsigned char HoutCnt[5];
TYPEOFTIME now(void)
{
  return ++__LABS_time;
}

void attr(TYPEOFAGENTID id, TYPEOFKEYIID key, TYPEOFVALUES value, Bool check)
{
  I[id][key] = value;
}

void env(TYPEOFAGENTID id, TYPEOFKEYEID key, TYPEOFVALUES value, Bool check)
{
  E[key] = value;
}

void init()
{
  TYPEOFVALUES _I[5][1];
  TYPEOFVALUES _E[2];
  TYPEOFPC _pc[5][1];
  unsigned char i;
  unsigned char j;
  for (i = 0; i < 5; i++)
  {
    terminated[i] = 0;
  }

  __VERIFIER_assume(((((_pc[0][0] == 2) || (_pc[0][0] == 3)) || (_pc[0][0] == 4)) || (_pc[0][0] == 5)) || (_pc[0][0] == 6));
  __VERIFIER_assume(((((_pc[1][0] == 2) || (_pc[1][0] == 3)) || (_pc[1][0] == 4)) || (_pc[1][0] == 5)) || (_pc[1][0] == 6));
  __VERIFIER_assume(((((_pc[2][0] == 2) || (_pc[2][0] == 3)) || (_pc[2][0] == 4)) || (_pc[2][0] == 5)) || (_pc[2][0] == 6));
  __VERIFIER_assume(((((_pc[3][0] == 2) || (_pc[3][0] == 3)) || (_pc[3][0] == 4)) || (_pc[3][0] == 5)) || (_pc[3][0] == 6));
  __VERIFIER_assume(((((_pc[4][0] == 2) || (_pc[4][0] == 3)) || (_pc[4][0] == 4)) || (_pc[4][0] == 5)) || (_pc[4][0] == 6));
  __VERIFIER_assume(_E[0] == (-128));
  __VERIFIER_assume(_E[1] == (-128));
  __VERIFIER_assume(_I[0][0] == 0);
  __VERIFIER_assume(_I[1][0] == 0);
  __VERIFIER_assume(_I[2][0] == 0);
  __VERIFIER_assume(_I[3][0] == 1);
  __VERIFIER_assume(_I[4][0] == 1);
  now();
  for (i = 0; i < 2; i++)
  {
    E[i] = _E[i];
  }

  for (i = 0; i < 5; i++)
  {
    for (j = 0; j < 1; j++)
    {
      pc[i][j] = _pc[i][j];
    }

    for (j = 0; j < 1; j++)
    {
      I[i][j] = _I[i][j];
    }

  }

}

void _0_2(int tid)
{
  __VERIFIER_assume(pc[tid][0] == 2);
  __VERIFIER_assume(I[tid][0] != 2);
  TYPEOFVALUES val0 = tid;
  TYPEOFVALUES val1 = I[tid][0];
  env(tid, 0, val0, 1);
  env(tid, 1, val1, 0);
  TYPEOFPC pc0;
  __VERIFIER_assume(((((pc0 == 2) || (pc0 == 3)) || (pc0 == 4)) || (pc0 == 5)) || (pc0 == 6));
  pc[tid][0] = pc0;
}

void _0_3(int tid)
{
  __VERIFIER_assume(pc[tid][0] == 3);
  __VERIFIER_assume((E[0] != tid) && (E[0] != (-128)));
  __VERIFIER_assume((E[1] != (-128)) && ((E[1] == 1) && (I[tid][0] == 2)));
  TYPEOFVALUES val0 = 1;
  attr(tid, 0, val0, 1);
  TYPEOFPC pc0;
  __VERIFIER_assume(((((pc0 == 2) || (pc0 == 3)) || (pc0 == 4)) || (pc0 == 5)) || (pc0 == 6));
  pc[tid][0] = pc0;
}

void _0_4(int tid)
{
  __VERIFIER_assume(pc[tid][0] == 4);
  __VERIFIER_assume((E[0] != tid) && (E[0] != (-128)));
  __VERIFIER_assume((E[1] != (-128)) && ((E[1] == 1) && (I[tid][0] == 0)));
  TYPEOFVALUES val0 = 2;
  attr(tid, 0, val0, 1);
  TYPEOFPC pc0;
  __VERIFIER_assume(((((pc0 == 2) || (pc0 == 3)) || (pc0 == 4)) || (pc0 == 5)) || (pc0 == 6));
  pc[tid][0] = pc0;
}

void _0_5(int tid)
{
  __VERIFIER_assume(pc[tid][0] == 5);
  __VERIFIER_assume((E[0] != tid) && (E[0] != (-128)));
  __VERIFIER_assume((E[1] != (-128)) && ((E[1] == 0) && (I[tid][0] == 1)));
  TYPEOFVALUES val0 = 2;
  attr(tid, 0, val0, 1);
  TYPEOFPC pc0;
  __VERIFIER_assume(((((pc0 == 2) || (pc0 == 3)) || (pc0 == 4)) || (pc0 == 5)) || (pc0 == 6));
  pc[tid][0] = pc0;
}

void _0_6(int tid)
{
  __VERIFIER_assume(pc[tid][0] == 6);
  __VERIFIER_assume((E[0] != tid) && (E[0] != (-128)));
  __VERIFIER_assume((E[1] != (-128)) && ((E[1] == 0) && (I[tid][0] == 2)));
  TYPEOFVALUES val0 = 0;
  attr(tid, 0, val0, 1);
  TYPEOFPC pc0;
  __VERIFIER_assume(((((pc0 == 2) || (pc0 == 3)) || (pc0 == 4)) || (pc0 == 5)) || (pc0 == 6));
  pc[tid][0] = pc0;
}

void monitor()
{
  assert(((((I[3][0] != 1) || (I[0][0] != 1)) || (I[4][0] != 1)) || (I[1][0] != 1)) || (I[2][0] != 1));
}

void finally()
{
}

int main(void)
{
  init();
  TYPEOFAGENTID firstAgent;
  while (1)
  {
    TYPEOFAGENTID newAgent;
    __VERIFIER_assume(newAgent < 5);
    firstAgent = newAgent;
    switch (pc[firstAgent][0])
    {
      case 2:
        _0_2(firstAgent);
        break;

      case 3:
        _0_3(firstAgent);
        break;

      case 4:
        _0_4(firstAgent);
        break;

      case 5:
        _0_5(firstAgent);
        break;

      case 6:
        _0_6(firstAgent);
        break;

      default:
        __VERIFIER_assume(0);

    }

    monitor();
  }

  finally();
}


