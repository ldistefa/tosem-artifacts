const char undef_value = -128;
const unsigned char BOUND = 0;
const unsigned char MAXCOMPONENTS = 3;
const unsigned char MAXPC = 1;

short __abs(short x) {
  return (x>0) ? x : -x;
}

short __max(short x, short y) { return (x > y) ? x : y; }
short __min(short x, short y) { return (x < y) ? x : y; }

short mod(short n, short m) {
  return n >= 0 ? n % m : m + (n % m);
}

const unsigned char MAXKEYI = 1;
short I[3][1];

unsigned char pc[3][1];


//
//  Rule ATTR
//  Component component_id  assigns to key the evaluated expression
//  If check is true, transition is guarded by HoutCnt == HinCnt == 0
//
void attr(unsigned char id, unsigned char key, short value, _Bool check) {


    I[id][key] = value;
}

const unsigned char MAXKEYE = 4;
short E[4];
void env(unsigned char id, unsigned char key, short value, _Bool check) {

    E[key] = value;
}


void init() {
    //short _I[MAXCOMPONENTS][MAXKEYI];
    unsigned char _pc[MAXCOMPONENTS][MAXPC];

    //short _E[MAXKEYE];

    //unsigned char i, j;


    __ESBMC_assume((_pc[0][0] == 14) || (_pc[0][0] == 15) || (_pc[0][0] == 18) || (_pc[0][0] == 20) || (_pc[0][0] == 21) || (_pc[0][0] == 22));

    __ESBMC_assume((_pc[1][0] == 14) || (_pc[1][0] == 15) || (_pc[1][0] == 18) || (_pc[1][0] == 20) || (_pc[1][0] == 21) || (_pc[1][0] == 22));

    __ESBMC_assume((_pc[2][0] == 3) || (_pc[2][0] == 4) || (_pc[2][0] == 7) || (_pc[2][0] == 9) || (_pc[2][0] == 10) || (_pc[2][0] == 11));
        
    I[0][0] = (0);
    I[1][0] = (0);
    I[2][0] = (1);

    E[0] = -1;
    E[1] = 0;
    E[2] = -1;
    E[3] = -1;
    
    pc[0][0] = _pc[0][0];
    pc[1][0] = _pc[1][0];
    pc[2][0] = _pc[2][0];

}

void _0_13(int tid) {
    //(lock, 1) <-- 3
    


    short val0 = 3;

    env(tid, 1, val0, 1);


    unsigned char pc0;
    __ESBMC_assume((pc0 == 14) || (pc0 == 15) || (pc0 == 18) || (pc0 == 20) || (pc0 == 21) || (pc0 == 22));
    pc[tid][0] = pc0;

}

void _0_14(int tid) {
    //((lock, 1)) == (2) and ((agent, 0)) == (id) and ((state, 0)) == (1) and ((responder, 3)) == (0)->(state, 0) <- 3
    


    __ESBMC_assume((((E[1]) == (2))) && (((E[0]) == (tid))));

    __ESBMC_assume((((I[tid][0]) == (1))) && (((E[3]) == (0))));

    short val0 = 3;

    attr(tid, 0, val0, 1);


    pc[tid][0] = 13;

}

void _0_15(int tid) {
    //((agent, 0)) != (id) and ((lock, 1)) == (0) and ((state, 0)) != (2)->(agent, 0),(message, 2),(lock, 1) <-- id,(state, 0),1
    


    __ESBMC_assume(((E[0]) != (tid)));

    __ESBMC_assume(((E[1]) == (0)));

    __ESBMC_assume(((I[tid][0]) != (2)));

    short val0 = tid;
    short val1 = I[tid][0];
    short val2 = 1;

    env(tid, 0, val0, 1);
    env(tid, 2, val1, 0);
    env(tid, 1, val2, 0);


    unsigned char pc0;
    __ESBMC_assume((pc0 == 14) || (pc0 == 15) || (pc0 == 18) || (pc0 == 20) || (pc0 == 21) || (pc0 == 22));
    pc[tid][0] = pc0;

}

void _0_16(int tid) {
    //((lock, 1)) == (3)->(agent, 0),(responder, 3),(lock, 1) <-- -(1),-(1),0
    


    __ESBMC_assume(((E[1]) == (3)));

    short val0 = -(1);
    short val1 = -(1);
    short val2 = 0;

    env(tid, 0, val0, 1);
    env(tid, 3, val1, 0);
    env(tid, 1, val2, 0);


    unsigned char pc0;
    __ESBMC_assume((pc0 == 14) || (pc0 == 15) || (pc0 == 18) || (pc0 == 20) || (pc0 == 21) || (pc0 == 22));
    pc[tid][0] = pc0;

}

void _0_17(int tid) {
    //(state, 0) <- 2
    


    short val0 = 2;

    attr(tid, 0, val0, 1);


    pc[tid][0] = 16;

}

void _0_18(int tid) {
    //((agent, 0)) != (id) and ((lock, 1)) == (1) and ((message, 2)) == (1) and ((state, 0)) == (0)->(lock, 1),(responder, 3) <-- 2,(state, 0)
    


    __ESBMC_assume(((E[0]) != (tid)));

    __ESBMC_assume(((E[1]) == (1)));

    __ESBMC_assume((((E[2]) == (1))) && (((I[tid][0]) == (0))));

    short val0 = 2;
    short val1 = I[tid][0];

    env(tid, 1, val0, 1);
    env(tid, 3, val1, 0);


    pc[tid][0] = 17;

}

void _0_19(int tid) {
    //(agent, 0),(lock, 1) <-- -(1),0
    


    short val0 = -(1);
    short val1 = 0;

    env(tid, 0, val0, 1);
    env(tid, 1, val1, 0);


    unsigned char pc0;
    __ESBMC_assume((pc0 == 14) || (pc0 == 15) || (pc0 == 18) || (pc0 == 20) || (pc0 == 21) || (pc0 == 22));
    pc[tid][0] = pc0;

}

void _0_20(int tid) {
    //((agent, 0)) != (id) and ((lock, 1)) == (1) and ((message, 2)) == (1) and ((state, 0)) == (2)->(state, 0) <- 3
    


    __ESBMC_assume(((E[0]) != (tid)));

    __ESBMC_assume(((E[1]) == (1)));

    __ESBMC_assume((((E[2]) == (1))) && (((I[tid][0]) == (2))));

    short val0 = 3;

    attr(tid, 0, val0, 1);


    pc[tid][0] = 19;

}

void _0_21(int tid) {
    //((agent, 0)) != (id) and ((lock, 1)) == (1) and ((message, 2)) == (0) and ((state, 0)) == (3)->(state, 0) <- 2
    


    __ESBMC_assume(((E[0]) != (tid)));

    __ESBMC_assume(((E[1]) == (1)));

    __ESBMC_assume((((E[2]) == (0))) && (((I[tid][0]) == (3))));

    short val0 = 2;

    attr(tid, 0, val0, 1);


    pc[tid][0] = 19;

}

void _0_22(int tid) {
    //((agent, 0)) != (id) and ((lock, 1)) == (1) and ((message, 2)) == (3) and ((state, 0)) == (2)->(state, 0) <- 3
    


    __ESBMC_assume(((E[0]) != (tid)));

    __ESBMC_assume(((E[1]) == (1)));

    __ESBMC_assume((((E[2]) == (3))) && (((I[tid][0]) == (2))));

    short val0 = 3;

    attr(tid, 0, val0, 1);


    pc[tid][0] = 19;

}

void _0_2(int tid) {
    //(lock, 1) <-- 3
    


    short val0 = 3;

    env(tid, 1, val0, 1);


    unsigned char pc0;
    __ESBMC_assume((pc0 == 3) || (pc0 == 4) || (pc0 == 7) || (pc0 == 9) || (pc0 == 10) || (pc0 == 11));
    pc[tid][0] = pc0;

}

void _0_3(int tid) {
    //((lock, 1)) == (2) and ((agent, 0)) == (id) and ((state, 0)) == (1) and ((responder, 3)) == (0)->(state, 0) <- 3
    


    __ESBMC_assume((((E[1]) == (2))) && (((E[0]) == (tid))));

    __ESBMC_assume((((I[tid][0]) == (1))) && (((E[3]) == (0))));

    short val0 = 3;

    attr(tid, 0, val0, 1);


    pc[tid][0] = 2;

}

void _0_4(int tid) {
    //((agent, 0)) != (id) and ((lock, 1)) == (0) and ((state, 0)) != (2)->(agent, 0),(message, 2),(lock, 1) <-- id,(state, 0),1
    


    __ESBMC_assume(((E[0]) != (tid)));

    __ESBMC_assume(((E[1]) == (0)));

    __ESBMC_assume(((I[tid][0]) != (2)));

    short val0 = tid;
    short val1 = I[tid][0];
    short val2 = 1;

    env(tid, 0, val0, 1);
    env(tid, 2, val1, 0);
    env(tid, 1, val2, 0);


    unsigned char pc0;
    __ESBMC_assume((pc0 == 3) || (pc0 == 4) || (pc0 == 7) || (pc0 == 9) || (pc0 == 10) || (pc0 == 11));
    pc[tid][0] = pc0;

}

void _0_5(int tid) {
    //((lock, 1)) == (3)->(agent, 0),(responder, 3),(lock, 1) <-- -(1),-(1),0
    


    __ESBMC_assume(((E[1]) == (3)));

    short val0 = -(1);
    short val1 = -(1);
    short val2 = 0;

    env(tid, 0, val0, 1);
    env(tid, 3, val1, 0);
    env(tid, 1, val2, 0);


    unsigned char pc0;
    __ESBMC_assume((pc0 == 3) || (pc0 == 4) || (pc0 == 7) || (pc0 == 9) || (pc0 == 10) || (pc0 == 11));
    pc[tid][0] = pc0;

}

void _0_6(int tid) {
    //(state, 0) <- 2
    


    short val0 = 2;

    attr(tid, 0, val0, 1);


    pc[tid][0] = 5;

}

void _0_7(int tid) {
    //((agent, 0)) != (id) and ((lock, 1)) == (1) and ((message, 2)) == (1) and ((state, 0)) == (0)->(lock, 1),(responder, 3) <-- 2,(state, 0)
    


    __ESBMC_assume(((E[0]) != (tid)));

    __ESBMC_assume(((E[1]) == (1)));

    __ESBMC_assume((((E[2]) == (1))) && (((I[tid][0]) == (0))));

    short val0 = 2;
    short val1 = I[tid][0];

    env(tid, 1, val0, 1);
    env(tid, 3, val1, 0);


    pc[tid][0] = 6;

}

void _0_8(int tid) {
    //(agent, 0),(lock, 1) <-- -(1),0
    


    short val0 = -(1);
    short val1 = 0;

    env(tid, 0, val0, 1);
    env(tid, 1, val1, 0);


    unsigned char pc0;
    __ESBMC_assume((pc0 == 3) || (pc0 == 4) || (pc0 == 7) || (pc0 == 9) || (pc0 == 10) || (pc0 == 11));
    pc[tid][0] = pc0;

}

void _0_9(int tid) {
    //((agent, 0)) != (id) and ((lock, 1)) == (1) and ((message, 2)) == (1) and ((state, 0)) == (2)->(state, 0) <- 3
    


    __ESBMC_assume(((E[0]) != (tid)));

    __ESBMC_assume(((E[1]) == (1)));

    __ESBMC_assume((((E[2]) == (1))) && (((I[tid][0]) == (2))));

    short val0 = 3;

    attr(tid, 0, val0, 1);


    pc[tid][0] = 8;

}

void _0_10(int tid) {
    //((agent, 0)) != (id) and ((lock, 1)) == (1) and ((message, 2)) == (0) and ((state, 0)) == (3)->(state, 0) <- 2
    


    __ESBMC_assume(((E[0]) != (tid)));

    __ESBMC_assume(((E[1]) == (1)));

    __ESBMC_assume((((E[2]) == (0))) && (((I[tid][0]) == (3))));

    short val0 = 2;

    attr(tid, 0, val0, 1);


    pc[tid][0] = 8;

}

void _0_11(int tid) {
    //((agent, 0)) != (id) and ((lock, 1)) == (1) and ((message, 2)) == (3) and ((state, 0)) == (2)->(state, 0) <- 3

    __ESBMC_assume(((E[0]) != (tid)));

    __ESBMC_assume(((E[1]) == (1)));

    __ESBMC_assume((((E[2]) == (3))) && (((I[tid][0]) == (2))));

    short val0 = 3;

    attr(tid, 0, val0, 1);


    pc[tid][0] = 8;

}

void monitor() {
    assert((((I[2][0]) == (0))) || (((I[2][0]) == (2))) || (((I[0][0]) == (0))) || (((I[0][0]) == (2))) || (((I[1][0]) == (0))) || (((I[1][0]) == (2))));
}


int main(void) {
    init();
    monitor(); // Check invariants on the initial state
    unsigned char firstAgent;
    __ESBMC_assume(firstAgent < MAXCOMPONENTS);
    ;


    while(1) {        
        // if (terminalState()) break;
        
            unsigned char nextAgent;
            __ESBMC_assume(nextAgent < MAXCOMPONENTS);
            firstAgent = nextAgent;

            switch (pc[firstAgent][0]) {
                case 13: _0_13(firstAgent); break;
                case 14: _0_14(firstAgent); break;
                case 15: _0_15(firstAgent); break;
                case 16: _0_16(firstAgent); break;
                case 17: _0_17(firstAgent); break;
                case 18: _0_18(firstAgent); break;
                case 19: _0_19(firstAgent); break;
                case 20: _0_20(firstAgent); break;
                case 21: _0_21(firstAgent); break;
                case 22: _0_22(firstAgent); break;
                case 2: _0_2(firstAgent); break;
                case 3: _0_3(firstAgent); break;
                case 4: _0_4(firstAgent); break;
                case 5: _0_5(firstAgent); break;
                case 6: _0_6(firstAgent); break;
                case 7: _0_7(firstAgent); break;
                case 8: _0_8(firstAgent); break;
                case 9: _0_9(firstAgent); break;
                case 10: _0_10(firstAgent); break;
                case 11: _0_11(firstAgent); break;
              default: 
                {}
            }
            
        monitor();

    }
}


