//extern void __VERIFIER_error() __attribute__ ((__noreturn__));
//extern void __VERIFIER_assume(int);
//void __VERIFIER_assert(int cond) { if(!(cond)) { ERROR: __VERIFIER_error(); } }
//extern int __VERIFIER_nondet_int(void);

typedef char TYPEOFVALUES;
typedef unsigned char TYPEOFPC;
typedef unsigned char TYPEOFTIME;
typedef unsigned char TYPEOFAGENTID;
typedef unsigned char TYPEOFKEYIID;
typedef unsigned char TYPEOFKEYLID;
typedef unsigned char TYPEOFKEYEID;
typedef unsigned char Bool;
TYPEOFVALUES __abs(TYPEOFVALUES x)
{
  return (x > 0) ? (x) : (-x);
}

TYPEOFVALUES __max(TYPEOFVALUES x, TYPEOFVALUES y)
{
  return (x > y) ? (x) : (y);
}

TYPEOFVALUES __min(TYPEOFVALUES x, TYPEOFVALUES y)
{
  return (x < y) ? (x) : (y);
}

TYPEOFVALUES mod(TYPEOFVALUES n, TYPEOFVALUES m)
{
  return (n >= 0) ? (n % m) : (m + (n % m));
}

TYPEOFVALUES I_0_0;
TYPEOFVALUES I_1_0;
TYPEOFVALUES I_2_0;
TYPEOFVALUES getI(int i, int j)
{
  switch (i)
  {
    case 0:
      return I_0_0;

    case 1:
      return I_1_0;

    case 2:
      return I_2_0;

    default:
    {
    }

  }

}

TYPEOFVALUES E_0;
TYPEOFVALUES E_1;
TYPEOFVALUES E_2;
TYPEOFVALUES E_3;
unsigned char pc_0_0;
unsigned char pc_1_0;
unsigned char pc_2_0;
TYPEOFVALUES getpc(int i, int j)
{
  switch (i)
  {
    case 0:
      return pc_0_0;

    case 1:
      return pc_1_0;

    case 2:
      return pc_2_0;

    default:
    {
    }

  }

}

void setpc(int i, int j, unsigned char value)
{
  switch (i)
  {
    case 0:
      pc_0_0 = value;
      break;

    case 1:
      pc_1_0 = value;
      break;

    case 2:
      pc_2_0 = value;
      break;

    default:
    {
    }

  }

}

void attr(TYPEOFAGENTID id, TYPEOFKEYIID key, TYPEOFVALUES value, Bool check)
{
  switch (id)
  {
    case 0:
      I_0_0 = value;
      break;

    case 1:
      I_1_0 = value;
      break;

    case 2:
      I_2_0 = value;
      break;

    default:
    {
    }

  }

}

void env(TYPEOFAGENTID id, TYPEOFKEYEID key, TYPEOFVALUES value, Bool check)
{
  switch (key)
  {
    case 0:
      E_0 = value;
      break;

    case 1:
      E_1 = value;
      break;

    case 2:
      E_2 = value;
      break;

    case 3:
      E_3 = value;
      break;

    default:
    {
    }

  }

}

void init()
{
  pc_0_0 = __VERIFIER_nondet_int();
  __VERIFIER_assume((((((pc_0_0 == 3) || (pc_0_0 == 4)) || (pc_0_0 == 7)) || (pc_0_0 == 9)) || (pc_0_0 == 11)) || (pc_0_0 == 13));
  pc_1_0 = __VERIFIER_nondet_int();
  __VERIFIER_assume((((((pc_1_0 == 3) || (pc_1_0 == 4)) || (pc_1_0 == 7)) || (pc_1_0 == 9)) || (pc_1_0 == 11)) || (pc_1_0 == 13));
  pc_2_0 = __VERIFIER_nondet_int();
  __VERIFIER_assume((((((pc_2_0 == 3) || (pc_2_0 == 4)) || (pc_2_0 == 7)) || (pc_2_0 == 9)) || (pc_2_0 == 11)) || (pc_2_0 == 13));
  E_0 = -1;
  E_1 = 0;
  E_2 = -1;
  E_3 = -1;
  I_0_0 = 0;
  I_1_0 = 0;
  I_2_0 = 1;
}

void _0_2(int tid)
{
  TYPEOFVALUES val0 = 3;
  TYPEOFPC pc0 = __VERIFIER_nondet_int();
  env(tid, 1, val0, 1);
  __VERIFIER_assume((((((pc0 == 3) || (pc0 == 4)) || (pc0 == 7)) || (pc0 == 9)) || (pc0 == 11)) || (pc0 == 13));
  setpc(tid, 0, pc0);
}

void _0_3(int tid)
{
  TYPEOFVALUES val0 = 3;
  __VERIFIER_assume((E_1 == 2) && (E_0 == tid));
  __VERIFIER_assume((getI(tid, 0) == 1) && (E_3 == 0));
  attr(tid, 0, val0, 1);
  setpc(tid, 0, 2);
}

void _0_4(int tid)
{
  TYPEOFVALUES val0 = tid;
  TYPEOFVALUES val1 = getI(tid, 0);
  TYPEOFVALUES val2 = 1;
  TYPEOFPC pc0 = __VERIFIER_nondet_int();
  __VERIFIER_assume(E_0 != tid);
  __VERIFIER_assume(E_1 == 0);
  __VERIFIER_assume(getI(tid, 0) != 2);
  env(tid, 0, val0, 1);
  env(tid, 2, val1, 0);
  env(tid, 1, val2, 0);
  __VERIFIER_assume((((((pc0 == 3) || (pc0 == 4)) || (pc0 == 7)) || (pc0 == 9)) || (pc0 == 11)) || (pc0 == 13));
  setpc(tid, 0, pc0);
}

void _0_5(int tid)
{
  TYPEOFVALUES val0 = -1;
  TYPEOFVALUES val1 = -1;
  TYPEOFVALUES val2 = 0;
  TYPEOFPC pc0 = __VERIFIER_nondet_int();
  __VERIFIER_assume(E_1 == 3);
  env(tid, 0, val0, 1);
  env(tid, 3, val1, 0);
  env(tid, 1, val2, 0);
  __VERIFIER_assume((((((pc0 == 3) || (pc0 == 4)) || (pc0 == 7)) || (pc0 == 9)) || (pc0 == 11)) || (pc0 == 13));
  setpc(tid, 0, pc0);
}

void _0_6(int tid)
{
  TYPEOFVALUES val0 = 2;
  attr(tid, 0, val0, 1);
  setpc(tid, 0, 5);
}

void _0_7(int tid)
{
  TYPEOFVALUES val0 = 2;
  TYPEOFVALUES val1 = getI(tid, 0);
  __VERIFIER_assume(E_0 != tid);
  __VERIFIER_assume(E_1 == 1);
  __VERIFIER_assume((E_2 == 1) && (getI(tid, 0) == 0));
  env(tid, 1, val0, 1);
  env(tid, 3, val1, 0);
  setpc(tid, 0, 6);
}

void _0_8(int tid)
{
  TYPEOFVALUES val0 = -1;
  TYPEOFVALUES val1 = 0;
  TYPEOFPC pc0 = __VERIFIER_nondet_int();
  env(tid, 0, val0, 1);
  env(tid, 1, val1, 0);
  __VERIFIER_assume((((((pc0 == 3) || (pc0 == 4)) || (pc0 == 7)) || (pc0 == 9)) || (pc0 == 11)) || (pc0 == 13));
  setpc(tid, 0, pc0);
}

void _0_9(int tid)
{
  TYPEOFVALUES val0 = 3;
  __VERIFIER_assume(E_0 != tid);
  __VERIFIER_assume(E_1 == 1);
  __VERIFIER_assume((E_2 == 1) && (getI(tid, 0) == 2));
  attr(tid, 0, val0, 1);
  setpc(tid, 0, 8);
}

void _0_10(int tid)
{
  TYPEOFPC pc0 = __VERIFIER_nondet_int();
  TYPEOFVALUES val0 = -1;
  TYPEOFVALUES val1 = 0;
  env(tid, 0, val0, 1);
  env(tid, 1, val1, 0);
  __VERIFIER_assume((((((pc0 == 3) || (pc0 == 4)) || (pc0 == 7)) || (pc0 == 9)) || (pc0 == 11)) || (pc0 == 13));
  setpc(tid, 0, pc0);
}

void _0_11(int tid)
{
  TYPEOFVALUES val0 = 2;
  __VERIFIER_assume(E_0 != tid);
  __VERIFIER_assume(E_1 == 1);
  __VERIFIER_assume((E_2 == 0) && (getI(tid, 0) == 3));
  attr(tid, 0, val0, 1);
  setpc(tid, 0, 10);
}

void _0_12(int tid)
{
  TYPEOFVALUES val0 = -1;
  TYPEOFVALUES val1 = 0;
  TYPEOFPC pc0 = __VERIFIER_nondet_int();
  __VERIFIER_assume(getpc(tid, 0) == 12);
  env(tid, 0, val0, 1);
  env(tid, 1, val1, 0);
  __VERIFIER_assume((((((pc0 == 3) || (pc0 == 4)) || (pc0 == 7)) || (pc0 == 9)) || (pc0 == 11)) || (pc0 == 13));
  setpc(tid, 0, pc0);
}

void _0_13(int tid)
{
  TYPEOFVALUES val0 = 3;
  __VERIFIER_assume(E_0 != tid);
  __VERIFIER_assume(E_1 == 1);
  __VERIFIER_assume((E_2 == 3) && (getI(tid, 0) == 2));
  attr(tid, 0, val0, 1);
  setpc(tid, 0, 12);
}

void monitor()
{
  __VERIFIER_assert((((((getI(2, 0) == 0) || (getI(2, 0) == 2)) || (getI(0, 0) == 0)) || (getI(0, 0) == 2)) || (getI(1, 0) == 0)) || (getI(1, 0) == 2));
}

void finally()
{
}

int main(void)
{
  TYPEOFAGENTID firstAgent;
  init();
  while (1)
  {
    TYPEOFAGENTID newagent = __VERIFIER_nondet_int();
    __VERIFIER_assume(((newagent == 0) || (newagent == 1)) || (newagent == 2));
    firstAgent = newagent;
    switch (getpc(firstAgent, 0))
    {
      case 2:
        _0_2(firstAgent);
        break;

      case 3:
        _0_3(firstAgent);
        break;

      case 4:
        _0_4(firstAgent);
        break;

      case 5:
        _0_5(firstAgent);
        break;

      case 6:
        _0_6(firstAgent);
        break;

      case 7:
        _0_7(firstAgent);
        break;

      case 8:
        _0_8(firstAgent);
        break;

      case 9:
        _0_9(firstAgent);
        break;

      case 10:
        _0_10(firstAgent);
        break;

      case 11:
        _0_11(firstAgent);
        break;

      case 12:
        _0_12(firstAgent);
        break;

      case 13:
        _0_13(firstAgent);
        break;

      default: {}
        //__VERIFIER_assert(0);

    }

    monitor();
  }

  finally();
}


