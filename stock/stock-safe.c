const char undef_value = -128;
#define AGENTS 1
#define SIZEOFPC 4
#define SIZEOFREPLICATED 0
#define SIZEOFSHARED 0

typedef char VALUE;
typedef unsigned char AGENT_ID;
typedef unsigned char VAR_INDEX;
typedef unsigned char PC_ELEMENT;

PC_ELEMENT pc[AGENTS][SIZEOFPC];

VALUE __abs(VALUE x) { return (x>0) ? x : -x; }
VALUE __max(VALUE x, VALUE y) { return (x > y) ? x : y; }
VALUE __min(VALUE x, VALUE y) { return (x < y) ? x : y; }
VALUE __mod(VALUE n, VALUE m) { return (n >= 0) ? n % m : m + (n % m); }

VALUE __nondetRange(VALUE minValue, VALUE bound) {
    VALUE x = __CPROVER_nondet();
    __CPROVER_assume((x >= minValue) & (x < bound));
    return x;
}

//TODO: other built-in functions




void init() {

    pc[0][0] = 42;
    pc[0][1] = 2;
    pc[0][2] = 23;
    pc[0][3] = 32;

}



_Bool action_0 (AGENT_ID id, _Bool sync) {
    /* #tau */
    

    pc[id][1] = 4;
    return 1;
}

_Bool action_1 (AGENT_ID id, _Bool sync) {
    /* #tau */
    

    pc[id][1] = 7;
    return 1;
}

_Bool action_10 (AGENT_ID id, _Bool sync) {
    /* change */
    

    pc[id][2] = 29;
    return 1;
}

_Bool action_11 (AGENT_ID id, _Bool sync) {
    /* check */
    

    pc[id][3] = 38;
    return 1;
}

_Bool action_12 (AGENT_ID id, _Bool sync) {
    /* save */
    

    pc[id][3] = 35;
    return 1;
}

_Bool action_13 (AGENT_ID id, _Bool sync) {
    /* check */
    

    pc[id][3] = 40;
    return 1;
}

_Bool action_14 (AGENT_ID id, _Bool sync) {
    /* save */
    

    pc[id][3] = 40;
    return 1;
}

_Bool action_15 (AGENT_ID id, _Bool sync) {
    /* #nop */
    

    pc[id][0] = 0;
    return 1;
}

_Bool action_16 (AGENT_ID id, _Bool sync) {
    /* bid_bk_mk */
    

    pc[id][1] = 18;
    pc[id][3] = 33;
    return 1;
}

_Bool action_17 (AGENT_ID id, _Bool sync) {
    /* notify_bd_bk */
    

    pc[id][1] = 0;
    pc[id][2] = 0;
    return 1;
}

_Bool action_18 (AGENT_ID id, _Bool sync) {
    /* result_mk_bd */
    

    pc[id][2] = 24;
    pc[id][3] = 0;
    return 1;
}

_Bool action_2 (AGENT_ID id, _Bool sync) {
    /* steel */
    

    pc[id][1] = 10;
    return 1;
}

_Bool action_3 (AGENT_ID id, _Bool sync) {
    /* iron */
    

    pc[id][1] = 10;
    return 1;
}

_Bool action_4 (AGENT_ID id, _Bool sync) {
    /* look */
    

    pc[id][1] = 11;
    return 1;
}

_Bool action_5 (AGENT_ID id, _Bool sync) {
    /* #tau */
    

    pc[id][1] = 17;
    return 1;
}

_Bool action_6 (AGENT_ID id, _Bool sync) {
    /* #tau */
    

    pc[id][1] = 10;
    return 1;
}

_Bool action_7 (AGENT_ID id, _Bool sync) {
    /* #tau */
    

    pc[id][1] = 19;
    return 1;
}

_Bool action_8 (AGENT_ID id, _Bool sync) {
    /* #tau */
    

    pc[id][2] = 29;
    return 1;
}

_Bool action_9 (AGENT_ID id, _Bool sync) {
    /* #tau */
    

    pc[id][2] = 27;
    return 1;
}

void noOp(AGENT_ID id) {
    // Perform all Nop triples
    for(char i=0; i < 1; ++i) {
        if (pc[id][0] == 42 & pc[id][1] == 0 & pc[id][2] == 0 & pc[id][3] == 0) { action_15(id, 0); }
    }
}


void agentFunction(AGENT_ID id) {
    _Bool done = 0;
    if (!done &(pc[id][1] == 2) & ((_Bool) __CPROVER_nondet()) ) { done = action_0(id, 0); }
    if (!done &(pc[id][1] == 2) & ((_Bool) __CPROVER_nondet()) ) { done = action_1(id, 0); }
    if (!done &(pc[id][1] == 4) & ((_Bool) __CPROVER_nondet()) ) { done = action_2(id, 0); }
    if (!done &(pc[id][1] == 7) & ((_Bool) __CPROVER_nondet()) ) { done = action_3(id, 0); }
    if (!done &(pc[id][1] == 10) & ((_Bool) __CPROVER_nondet()) ) { done = action_4(id, 0); }
    if (!done &(pc[id][1] == 11) & ((_Bool) __CPROVER_nondet()) ) { done = action_5(id, 0); }
    if (!done &(pc[id][1] == 11) & ((_Bool) __CPROVER_nondet()) ) { done = action_6(id, 0); }
    if (!done &(pc[id][1] == 18) & ((_Bool) __CPROVER_nondet()) ) { done = action_7(id, 0); }
    if (!done &(pc[id][2] == 24) & ((_Bool) __CPROVER_nondet()) ) { done = action_8(id, 0); }
    if (!done &(pc[id][2] == 24) & ((_Bool) __CPROVER_nondet()) ) { done = action_9(id, 0); }
    if (!done &(pc[id][2] == 27) & ((_Bool) __CPROVER_nondet()) ) { done = action_10(id, 0); }
    if (!done &(pc[id][3] == 33) & ((_Bool) __CPROVER_nondet()) ) { done = action_11(id, 0); }
    if (!done &(pc[id][3] == 33) & ((_Bool) __CPROVER_nondet()) ) { done = action_12(id, 0); }
    if (!done &(pc[id][3] == 35) & ((_Bool) __CPROVER_nondet()) ) { done = action_13(id, 0); }
    if (!done &(pc[id][3] == 38) & ((_Bool) __CPROVER_nondet()) ) { done = action_14(id, 0); }
    if (!done &(pc[id][1] == 17) & (pc[id][3] == 32) & ((_Bool) __CPROVER_nondet()) ) { done = action_16(id, 0); }
    if (!done &(pc[id][1] == 19) & (pc[id][2] == 29) & ((_Bool) __CPROVER_nondet()) ) { done = action_17(id, 0); }
    if (!done &(pc[id][2] == 23) & (pc[id][3] == 40) & ((_Bool) __CPROVER_nondet()) ) { done = action_18(id, 0); }
    __CPROVER_assume(done);
}
VALUE B;
_Bool live=0, saved=0;
VALUE steps = 0;
_Bool p0() {
	//assertion goes here
}


int main(void) {
    init();
    AGENT_ID next = 0;
    while(1) {
        steps++; //number steps so far
        noOp(next);
        agentFunction(next);
        noOp(next);
	// EI version 
	live = saved?(live && p0()):(live || p0()); if (!saved && live) saved = 1; __CPROVER_assert((!(steps == B))  || live, "E&I");
        next = __CPROVER_nondet();
        __CPROVER_assume(next < AGENTS);
    }

}

