extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int(void);typedef char VALUE;
typedef unsigned char AGENT_ID;
typedef unsigned char VAR_INDEX;
typedef unsigned char PC_ELEMENT;
const char undef_value = -128;
PC_ELEMENT pc_0;
PC_ELEMENT pc_1;
PC_ELEMENT pc_2;
PC_ELEMENT pc_3;
VALUE __abs(VALUE x)
{
  return (x > 0) ? (x) : (-x);
}

VALUE __max(VALUE x, VALUE y)
{
  return (x > y) ? (x) : (y);
}

VALUE __min(VALUE x, VALUE y)
{
  return (x < y) ? (x) : (y);
}

VALUE __mod(VALUE n, VALUE m)
{
  return (n >= 0) ? (n % m) : (m + (n % m));
}

VALUE __nondetRange(VALUE minValue, VALUE bound)
{
  VALUE x = __VERIFIER_nondet_int();
  __VERIFIER_assume((x >= minValue) && (x < bound));
  return x;
}

void init()
{
  pc_0 = 37;
  pc_1 = 2;
  pc_2 = 19;
  pc_3 = 27;
}

void monitor(void)
{
  assert(((((((((((pc_1 == 2) || (pc_1 == 6)) || (pc_1 == 7)) || (pc_1 == 14)) || (pc_2 == 20)) || (pc_3 == 28)) || (pc_3 == 30)) || (pc_3 == 33)) || ((pc_1 == 13) && (pc_3 == 27))) || ((pc_1 == 14) && (pc_2 == 23))) || ((pc_2 == 19) && (pc_3 == 35)));
}

PC_ELEMENT getpc(int x0, int x1)
{
  switch ((4 * x0) + x1)
  {
    case 0:
      return pc_0;

    case 1:
      return pc_1;

    case 2:
      return pc_2;

    case 3:
      return pc_3;

  }

}

void setpc(int x0, int x1, PC_ELEMENT value)
{
  switch ((4 * x0) + x1)
  {
    case 0:
      pc_0 = value;
      break;

    case 1:
      pc_1 = value;
      break;

    case 2:
      pc_2 = value;
      break;

    case 3:
      pc_3 = value;
      break;

  }

}

_Bool action_11(AGENT_ID id, _Bool sync)
{
  setpc(id, 3, 35);
  return 1;
}

_Bool action_6(AGENT_ID id, _Bool sync)
{
  setpc(id, 2, 0);
  return 1;
}

_Bool action_15(AGENT_ID id, _Bool sync)
{
  setpc(id, 2, 20);
  setpc(id, 3, 0);
  return 1;
}

_Bool action_10(AGENT_ID id, _Bool sync)
{
  setpc(id, 3, 35);
  return 1;
}

_Bool action_5(AGENT_ID id, _Bool sync)
{
  setpc(id, 1, 0);
  return 1;
}

_Bool action_14(AGENT_ID id, _Bool sync)
{
  setpc(id, 1, 0);
  setpc(id, 2, 0);
  return 1;
}

_Bool action_1(AGENT_ID id, _Bool sync)
{
  setpc(id, 1, 6);
  return 1;
}

_Bool action_4(AGENT_ID id, _Bool sync)
{
  setpc(id, 1, 6);
  return 1;
}

_Bool action_9(AGENT_ID id, _Bool sync)
{
  setpc(id, 3, 30);
  return 1;
}

_Bool action_13(AGENT_ID id, _Bool sync)
{
  setpc(id, 1, 14);
  setpc(id, 3, 28);
  return 1;
}

_Bool action_0(AGENT_ID id, _Bool sync)
{
  setpc(id, 1, 6);
  return 1;
}

_Bool action_3(AGENT_ID id, _Bool sync)
{
  setpc(id, 1, 13);
  return 1;
}

_Bool action_8(AGENT_ID id, _Bool sync)
{
  setpc(id, 3, 33);
  return 1;
}

_Bool action_12(AGENT_ID id, _Bool sync)
{
  setpc(id, 0, 0);
  return 1;
}

void noOp(AGENT_ID id)
{
  for (char i = 0; i < 1; ++i)
  {
    if ((((getpc(id, 0) == 37) && (getpc(id, 1) == 0)) && (getpc(id, 2) == 0)) && (getpc(id, 3) == 0))
    {
      action_12(id, 0);
    }

  }

}

_Bool action_2(AGENT_ID id, _Bool sync)
{
  setpc(id, 1, 7);
  return 1;
}

_Bool action_7(AGENT_ID id, _Bool sync)
{
  setpc(id, 2, 23);
  return 1;
}

void agentFunction(AGENT_ID id)
{
  _Bool done = 0;
  if (((!done) && (getpc(id, 1) == 2)) && ((_Bool) __VERIFIER_nondet_int()))
  {
    done = action_0(id, 0);
  }

  if (((!done) && (getpc(id, 1) == 2)) && ((_Bool) __VERIFIER_nondet_int()))
  {
    done = action_1(id, 0);
  }

  if (((!done) && (getpc(id, 1) == 6)) && ((_Bool) __VERIFIER_nondet_int()))
  {
    done = action_2(id, 0);
  }

  if (((!done) && (getpc(id, 1) == 7)) && ((_Bool) __VERIFIER_nondet_int()))
  {
    done = action_3(id, 0);
  }

  if (((!done) && (getpc(id, 1) == 7)) && ((_Bool) __VERIFIER_nondet_int()))
  {
    done = action_4(id, 0);
  }

  if (((!done) && (getpc(id, 1) == 14)) && ((_Bool) __VERIFIER_nondet_int()))
  {
    done = action_5(id, 0);
  }

  if (((!done) && (getpc(id, 2) == 20)) && ((_Bool) __VERIFIER_nondet_int()))
  {
    done = action_6(id, 0);
  }

  if (((!done) && (getpc(id, 2) == 20)) && ((_Bool) __VERIFIER_nondet_int()))
  {
    done = action_7(id, 0);
  }

  if (((!done) && (getpc(id, 3) == 28)) && ((_Bool) __VERIFIER_nondet_int()))
  {
    done = action_8(id, 0);
  }

  if (((!done) && (getpc(id, 3) == 28)) && ((_Bool) __VERIFIER_nondet_int()))
  {
    done = action_9(id, 0);
  }

  if (((!done) && (getpc(id, 3) == 30)) && ((_Bool) __VERIFIER_nondet_int()))
  {
    done = action_10(id, 0);
  }

  if (((!done) && (getpc(id, 3) == 33)) && ((_Bool) __VERIFIER_nondet_int()))
  {
    done = action_11(id, 0);
  }

  if ((((!done) && (getpc(id, 1) == 13)) && (getpc(id, 3) == 27)) && ((_Bool) __VERIFIER_nondet_int()))
  {
    done = action_13(id, 0);
  }

  if ((((!done) && (getpc(id, 1) == 14)) && (getpc(id, 2) == 23)) && ((_Bool) __VERIFIER_nondet_int()))
  {
    done = action_14(id, 0);
  }

  if ((((!done) && (getpc(id, 2) == 19)) && (getpc(id, 3) == 35)) && ((_Bool) __VERIFIER_nondet_int()))
  {
    done = action_15(id, 0);
  }

  __VERIFIER_assume(done);
}

int main(void)
{
  init();
  AGENT_ID next = 0;
  while (1)
  {
    noOp(next);
    agentFunction(next);
    noOp(next);
    monitor();
    next = __VERIFIER_nondet_int();
    __VERIFIER_assume(next < 1);
  }

}


