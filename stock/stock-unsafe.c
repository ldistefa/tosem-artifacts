const char undef_value = -128;
#define AGENTS 1
#define SIZEOFPC 4
#define SIZEOFREPLICATED 0
#define SIZEOFSHARED 0

typedef char VALUE;
typedef unsigned char AGENT_ID;
typedef unsigned char VAR_INDEX;
typedef unsigned char PC_ELEMENT;

PC_ELEMENT pc[AGENTS][SIZEOFPC];

VALUE __abs(VALUE x) { return (x>0) ? x : -x; }
VALUE __max(VALUE x, VALUE y) { return (x > y) ? x : y; }
VALUE __min(VALUE x, VALUE y) { return (x < y) ? x : y; }
VALUE __mod(VALUE n, VALUE m) { return (n >= 0) ? n % m : m + (n % m); }

VALUE __nondetRange(VALUE minValue, VALUE bound) {
    VALUE x = __CPROVER_nondet();
    __CPROVER_assume((x >= minValue) & (x < bound));
    return x;
}

//TODO: other built-in functions




void init() {

    pc[0][0] = 37;
    pc[0][1] = 2;
    pc[0][2] = 19;
    pc[0][3] = 27;

}



_Bool action_0 (AGENT_ID id, _Bool sync) {
    /* iron */
    

    pc[id][1] = 6;
    return 1;
}

_Bool action_1 (AGENT_ID id, _Bool sync) {
    /* steel */
    

    pc[id][1] = 6;
    return 1;
}

_Bool action_10 (AGENT_ID id, _Bool sync) {
    /* check */
    

    pc[id][3] = 35;
    return 1;
}

_Bool action_11 (AGENT_ID id, _Bool sync) {
    /* save */
    

    pc[id][3] = 35;
    return 1;
}

_Bool action_12 (AGENT_ID id, _Bool sync) {
    /* #nop */
    

    pc[id][0] = 0;
    return 1;
}

_Bool action_13 (AGENT_ID id, _Bool sync) {
    /* bid_bk_mk */
    

    pc[id][1] = 14;
    pc[id][3] = 28;
    return 1;
}

_Bool action_14 (AGENT_ID id, _Bool sync) {
    /* notify_bd_bk */
    

    pc[id][1] = 0;
    pc[id][2] = 0;
    return 1;
}

_Bool action_15 (AGENT_ID id, _Bool sync) {
    /* result_mk_bd */
    

    pc[id][2] = 20;
    pc[id][3] = 0;
    return 1;
}

_Bool action_2 (AGENT_ID id, _Bool sync) {
    /* look */
    

    pc[id][1] = 7;
    return 1;
}

_Bool action_3 (AGENT_ID id, _Bool sync) {
    /* #tau */
    

    pc[id][1] = 13;
    return 1;
}

_Bool action_4 (AGENT_ID id, _Bool sync) {
    /* #tau */
    

    pc[id][1] = 6;
    return 1;
}

_Bool action_5 (AGENT_ID id, _Bool sync) {
    /* #tau */
    

    pc[id][1] = 0;
    return 1;
}

_Bool action_6 (AGENT_ID id, _Bool sync) {
    /* #tau */
    

    pc[id][2] = 0;
    return 1;
}

_Bool action_7 (AGENT_ID id, _Bool sync) {
    /* change */
    

    pc[id][2] = 23;
    return 1;
}

_Bool action_8 (AGENT_ID id, _Bool sync) {
    /* check */
    

    pc[id][3] = 33;
    return 1;
}

_Bool action_9 (AGENT_ID id, _Bool sync) {
    /* save */
    

    pc[id][3] = 30;
    return 1;
}

void noOp(AGENT_ID id) {
    // Perform all Nop triples
    for(char i=0; i < 1; ++i) {
        if (pc[id][0] == 37 & pc[id][1] == 0 & pc[id][2] == 0 & pc[id][3] == 0) { action_12(id, 0); }
    }
}


void agentFunction(AGENT_ID id) {
    _Bool done = 0;
    if (!done &(pc[id][1] == 2) & ((_Bool) __CPROVER_nondet()) ) { done = action_0(id, 0); }
    if (!done &(pc[id][1] == 2) & ((_Bool) __CPROVER_nondet()) ) { done = action_1(id, 0); }
    if (!done &(pc[id][1] == 6) & ((_Bool) __CPROVER_nondet()) ) { done = action_2(id, 0); }
    if (!done &(pc[id][1] == 7) & ((_Bool) __CPROVER_nondet()) ) { done = action_3(id, 0); }
    if (!done &(pc[id][1] == 7) & ((_Bool) __CPROVER_nondet()) ) { done = action_4(id, 0); }
    if (!done &(pc[id][1] == 14) & ((_Bool) __CPROVER_nondet()) ) { done = action_5(id, 0); }
    if (!done &(pc[id][2] == 20) & ((_Bool) __CPROVER_nondet()) ) { done = action_6(id, 0); }
    if (!done &(pc[id][2] == 20) & ((_Bool) __CPROVER_nondet()) ) { done = action_7(id, 0); }
    if (!done &(pc[id][3] == 28) & ((_Bool) __CPROVER_nondet()) ) { done = action_8(id, 0); }
    if (!done &(pc[id][3] == 28) & ((_Bool) __CPROVER_nondet()) ) { done = action_9(id, 0); }
    if (!done &(pc[id][3] == 30) & ((_Bool) __CPROVER_nondet()) ) { done = action_10(id, 0); }
    if (!done &(pc[id][3] == 33) & ((_Bool) __CPROVER_nondet()) ) { done = action_11(id, 0); }
    if (!done &(pc[id][1] == 13) & (pc[id][3] == 27) & ((_Bool) __CPROVER_nondet()) ) { done = action_13(id, 0); }
    if (!done &(pc[id][1] == 14) & (pc[id][2] == 23) & ((_Bool) __CPROVER_nondet()) ) { done = action_14(id, 0); }
    if (!done &(pc[id][2] == 19) & (pc[id][3] == 35) & ((_Bool) __CPROVER_nondet()) ) { done = action_15(id, 0); }
    __CPROVER_assume(done);
}

void monitor(void) {
	__CPROVER_assert(
        (pc[0][1] == 2) | 
        (pc[0][1] == 6) | 
        (pc[0][1] == 7) | 
        (pc[0][1] == 14) |
        (pc[0][2] == 20) |
        (pc[0][3] == 28) |
        (pc[0][3] == 30) |
        (pc[0][3] == 33) |
        ((pc[0][1] == 13) & (pc[0][3] == 27)) |
        ((pc[0][1] == 14) & (pc[0][2] == 23)) |
        ((pc[0][2] == 19) & (pc[0][3] == 35)), "noDeadlock"
    );
}

int main(void) {
    init();
    AGENT_ID next = 0;
    while(1) {
        noOp(next);
        agentFunction(next);
        noOp(next);
        monitor();
        next = __CPROVER_nondet();
        __CPROVER_assume(next < AGENTS);
    }

}

